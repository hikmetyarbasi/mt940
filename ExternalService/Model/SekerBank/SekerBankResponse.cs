﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ExternalService.Model.SekerBank
{
    [Serializable, XmlRoot("Accounts")]
    public class SekerBankResponse
    {
        [XmlElement("Account")]
        public List<Account> Accounts { get; set; }
    }
    
    public class Account
    {
        public string HesapDurumu { get; set; }
        public string MusteriNo { get; set; }
        public string MusteriAdi { get; set; }
        public string HesapNo { get; set; }
        public string SubeNo { get; set; }
        public string SubeAdi { get; set; }
        public string DovizKodu { get; set; }
        public string Bakiye { get; set; }
        public string KullanilabilirBakiye { get; set; }
        public string HesapKapanisTarihi { get; set; }
        public string HesapAcilisTarihi { get; set; }
        public string SonHareketTarihi { get; set; }
        public string IBAN { get; set; }
        public string HesapTuru { get; set; }
        public string HesapAdi { get; set; }
        public string VadeTarihi { get; set; }
        public string FaizOrani { get; set; }
        public string KrediLimiti { get; set; }
        public string KrediliKullanilabilirBakiye { get; set; }
        public string BlokeTutari { get; set; }
        [XmlElement("HesapHareketleri")]
        public List<Hareket> HesapHareketleri { get; set; }
    }

    public class Hareket
    {
        public string HareketTarihi { get; set; }
        public string HareketSaati { get; set; }
        public string SiraNo { get; set; }
        public string HareketTutari { get; set; }
        public string SubeKodu { get; set; }
        public string DovizKodu { get; set; }
        public string BorcAlacak { get; set; }
        public string AcilisBakiye { get; set; }
        public string KapanisBakiye { get; set; }
        public string Aciklama { get; set; }
        public string MusteriNo { get; set; }
        public string IslemID { get; set; }
        public string IslemKodu { get; set; }
        public string ReferansNo { get; set; }
        public string KarsiHesapVKNTCKN { get; set; }
        public string DekontNo { get; set; }
        public string KanalKodu { get; set; }
        public string KanalAdi { get; set; }
        public string EkDetay1 { get; set; }
        public string EkDetay2 { get; set; }
        public string EkDetay3 { get; set; }
        public string EkDetay4 { get; set; }

    }
}
