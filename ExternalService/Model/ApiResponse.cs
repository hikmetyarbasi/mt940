﻿namespace ExternalServices.Model
{
    public class ApiResponse<T>
    {
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public T ResponseObject { get; set; }

        public int TryCount { get; set; }

    }
}
