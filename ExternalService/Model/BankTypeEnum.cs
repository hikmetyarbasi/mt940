﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalService.Model
{
    public enum BankTypeEnum
    {
        Akbank=046,
        Denizbank = 134,
        Finansbank = 111,
        Garanti = 062,
        IsBankası = 064,
        TEBBankasi = 032,
        Vakifbank = 015,
        Yapikredi = 067,
        ZiraatBankasi = 010,
        Halkbankasi = 012,
        Kuveytturk = 205,
        Albaraka = 203,
        Sekerbank = 059,
        Hsbc = 001
    }
}
