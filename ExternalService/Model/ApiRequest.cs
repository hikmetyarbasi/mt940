﻿namespace ExternalServices.Model
{
    public class ApiRequest<T>
    {
        public T RequestedObject { get; set; }
    }
}
