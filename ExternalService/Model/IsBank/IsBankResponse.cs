﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ExternalService.Model.IsBank
{
    [Serializable, XmlRoot("XMLEXBAT")]
    public class IsBankApiResponse
    {
        public string Tarih { get; set; }
        public string Saat { get; set; }
        public string bankaKodu { get; set; }
        public string bankaAdi { get; set; }
        public string bankaVergiDairesi { get; set; }
        public string bankaVergiNumarasi { get; set; }
        public List<Hesap> Hesaplar { get; set; }
    }

    public class Hesap
    {
        public Tanimlama Tanimlamalar { get; set; }
        public List<Hareket> Hareketler { get; set; }
    }

    public class Tanimlama
    {
        public string IbanNo { get; set; }
        public string HesapTuru { get; set; }
        public string HesapNo { get; set; }
        public string MusteriNo { get; set; }
        public string SubeKodu { get; set; }
        public string SubeAdi { get; set; }
        public string DovizTuru { get; set; }
        public string HesapAcilisTarihi { get; set; }
        public string SonHareketTarihi { get; set; }
        public string Bakiye { get; set; }
        public string AcilisBakiyesi { get; set; }
        public string BlokeMeblag { get; set; }
        public string KullanilabilirBakiye { get; set; }
        public string KrediLimiti { get; set; }
        public string KrediliKullanilabilirBakiye { get; set; }
    }

    public class Hareket
    {
        public string Tarih { get; set; }
        public string HareketSirano { get; set; }
        public string Miktar { get; set; }
        public string Harislemid2 { get; set; }
        public string Bakiye { get; set; }
        public string Aciklama { get; set; }
        public string valor { get; set; }
        public string timeStamp { get; set; }
        public string ISL_Saat { get; set; }
        public string BankaKodu { get; set; }
        public string ParaBirimi { get; set; }
        public string Kaynak { get; set; }
        public string IslemKodu { get; set; }
        public string borcAlacak { get; set; }
        public string IslemTuru { get; set; }
        public string KarsiHesapVKN { get; set; }
        public string DekontNo { get; set; }
        public string KarsiHesSahipAdUnvan { get; set; }
        public string referans { get; set; }
        public string KarsiBanka { get; set; }
        public string KarsiSube { get; set; }
        public string KarsiHesap { get; set; }
        public string KarsiBankaAdi { get; set; }
        public string IslemiYapanSubeKodu { get; set; }
        public string IslemiYapanSubeAdi { get; set; }
        public string IslemTod { get; set; }
        public string EFTSorguNo { get; set; }
        public string IthalatAciklamasi { get; set; }
    }
}
