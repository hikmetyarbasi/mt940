﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ExternalService.Model.Teb
{
    [XmlRoot("HESHARSORGU")]
    public class RequestModel
    {

        XmlSerializer _xmlSerializer = new XmlSerializer(typeof(RequestModel));
        public string FIRMA_AD { get; set; }

        public string FIRMA_ANAHTAR { get; set; }
        public string SUBENO { get; set; }
        public string HESNO { get; set; }
        public string BASTAR { get; set; }
        public string BITTAR { get; set; }
        public string GON_IBAN_EH { get; set; }
        public string SON_BKY_EH { get; set; }
        public string DEKONTNO_EH { get; set; }
        public string BLOKE_BKY_EH { get; set; }
        public string ALICI_IBAN_EH { get; set; }

        public string WrapInCData()
        {
            MemoryStream ms = new MemoryStream();
            _xmlSerializer.Serialize(ms, this);
            return Encoding.UTF8.GetString(ms.ToArray());
        }
        public void WriteXml(XmlWriter writer)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter innerWriter = XmlWriter.Create(ms, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    _xmlSerializer.Serialize(innerWriter, this);
                    innerWriter.Flush();
                    writer.WriteCData(Encoding.UTF8.GetString(ms.ToArray()));
                }
            }
        }
    }
}
