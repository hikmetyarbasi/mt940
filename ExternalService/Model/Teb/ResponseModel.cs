﻿using ExternalServices.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ExternalService.Model.Teb
{
    [Serializable, XmlRoot("HAREKETLER")]
    public class TebResponse
    {
        [XmlElement("SUBENO")]
        public string Subeno { get; set; }

        [XmlElement("HESNO")]
        public string Hesno { get; set; }

        [XmlElement("BASTAR")]
        public string Bastar { get; set; }

        [XmlElement("BITTAR")]
        public string Bittar { get; set; }

        [XmlElement("DETAYLAR")]
        public Detaylar Detaylar { get; set; }
    }


    public partial class Detaylar
    {
        [XmlElement("DETAY")]
        public Detay[] Detay { get; set; }
    }

    public partial class Detay
    {
        [XmlElement("HAREKET_KEY")]
        public string HareketKey { get; set; }

        [XmlElement("ISLEM_TAR")]
        public string IslemTar { get; set; }

        [XmlElement("BA")]
        public string Ba { get; set; }

        [XmlElement("PARAKOD")]
        public string Parakod { get; set; }

        [XmlElement("TUTAR")]
        public string Tutar { get; set; }

        [XmlElement("ACIKLAMA")]
        public string Aciklama { get; set; }

        [XmlElement("MUSTERI_REF")]
        public string MusteriRef { get; set; }

        [XmlElement("GONDEREN_AD")]
        public string GonderenAd { get; set; }

        [XmlElement("GONDEREN_BANKA")]
        public string GonderenBanka { get; set; }

        [XmlElement("GONDEREN_SUBE")]
        public string GonderenSube { get; set; }

        [XmlElement("ISLEM_TAR_SAAT")]
        public string IslemTarSaat { get; set; }

        [XmlElement("ANLIK_BKY")]
        public string AnlikBky { get; set; }

        [XmlElement("ISLEM_ACK")]
        public string IslemAck { get; set; }

        [XmlElement("ISLEM_TUR")]
        public long IslemTur { get; set; }

        [XmlElement("BORCLU_VKN")]
        public string BorcluVkn { get; set; }

        [XmlElement("ALACAKLI_VKN")]
        public string AlacakliVkn { get; set; }

        [XmlElement("GONDEREN_IBAN")]
        public string GonderenIban { get; set; }

        [XmlElement("DEKONT_NO")]
        public string DekontNo { get; set; }
        [XmlElement("ALICI_IBAN")]
        public string AliciIban { get; set; }
        
    }
}
