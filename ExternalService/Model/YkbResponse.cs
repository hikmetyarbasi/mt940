﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using tr.com.yapikredi.api;

namespace ExternalService.Model
{
    public class YkbResponse
    {
        [JsonProperty("return")]
        public responseEhoAccountTransaction @return { get; set; }
    }
}
