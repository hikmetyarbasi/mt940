﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalService.Model.Finansbank
{
  public class FinansbankUserInfo
    {
        public FbUser[] Users { get; set; }
    }
    public class FbUser
    {
        public string AppUserName { get; set; }
        public string AppPassword { get; set; }
        public Detail Details { get; set; }
    }
    public class Detail
    {
        public string FirmCode { get; set; }
    }
}
