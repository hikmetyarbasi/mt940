﻿using Newtonsoft.Json;

namespace ExternalService.Model
{
    public class AppUserInfo
    {
        public User[] Users { get; set; }

    }
    public class User
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string BankType { get; set; }
    }
}
