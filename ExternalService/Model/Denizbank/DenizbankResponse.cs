﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using tr.com.denizbank.api;

namespace ExternalService.Model.Denizbank
{


    [Serializable, XmlRoot("Accounts")]
    public class DenizbankResponse
    {
        [XmlElement("BankaHesaplariClassDetail")]
        public List<BankaHesaplariClassDetail> BankaHesaplariClassDetail { get; set; }
    }
}
