﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalService.Model.IsBank
{
    public class IsBankaUserInfo
    {
        public IsBankaUser[] Users { get; set; }
    }
    public class IsBankaUser
    {
        public string AppUserName { get; set; }
        public Detail UserDetail { get; set; }
    }
    public class Detail
    {
        public string UserName { get; set; }
        public string MusteriNo { get; set; }
        public string Password { get; set; }
    }
}
