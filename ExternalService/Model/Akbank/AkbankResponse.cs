﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ExternalService.Model.Akbank
{

    [Serializable, XmlRoot("Hesaps")]
    public class AkbankResponse
    {
        [XmlElement("Hesap")]
        public List<Hesap> Hesaps { get; set; }
    }

    public class Hesap
    {

        public string SqlID { get; set; }
        public string HesapTuruAdi { get; set; }
        public string HesapNo { get; set; }
        public string URF { get; set; }
        public string SubeKodu { get; set; }
        public string SubeAdi { get; set; }
        public string DovizKodu { get; set; }
        public string IBAN { get; set; }
        public string AcilisIlkBakiye { get; set; }
        public string AcilisGunBakiye { get; set; }
        public string CariBakiye { get; set; }
        public string Bakiye { get; set; }
        public string BlokeMeblag { get; set; }
        public string HesapAcilisTarihi { get; set; }
        public string SonHareketTarihi { get; set; }
        public string HesapTuruKodu { get; set; }
        public string LastTmSt { get; set; }
        public string AktifFlag { get; set; }
        public string DetayFlag { get; set; }
        public string DekontBilgiFlag { get; set; }
        public string VadeliHareketlerIslensin { get; set; }
        [XmlElement("Detay")]
        public List<Detay> Detay { get; set; }
    }

    public class Detay
    {
        public string ValorTarihi { get; set; }
        public string IslemTarihi { get; set; }
        public string ReferansNo { get; set; }
        public string KarsiHesapIBAN { get; set; }
        public string Tutar { get; set; }
        public string SonBakiye { get; set; }
        public string Aciklama { get; set; }
        public string VKN { get; set; }
        public string TutarBorcAlacak { get; set; }
        public string SonBakiyeBorcAlacak { get; set; }
        public string FonksiyonKodu { get; set; }
        public string MT940FonksiyonKodu { get; set; }
        public string FisNo { get; set; }
        public string OzelAlan1 { get; set; }
        public string OzelAlan2 { get; set; }
        public string AboneNo { get; set; }
        public string FaturaNo { get; set; }
        public string FaturaDonem { get; set; }
        public string FaturaSonOdemeTarihi { get; set; }
        public string LehdarVKN { get; set; }
        public string LehdarTCKN { get; set; }
        public string AmirVKN { get; set; }
        public string AmirTCKN { get; set; }
        public string BorcluIBAN { get; set; }
        public string AlacakliIBAN { get; set; }
        public string DekontBorcAciklama { get; set; }
        public string DekontAlacakAciklama { get; set; }
        public string IslemiYapanSube { get; set; }
        public string HareketDurumu { get; set; }
        public string TimeStamp { get; set; }

    }
}
