﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalService.Services.Abstract
{
    public static class ExtMapMethod
    {
        public static string ToMapAmountCariBakiye(this string amount)
        {
            var ek = amount.Trim().StartsWith("-") ? "-" : "";
            amount = amount.Replace(",", ".");
            if (!amount.Contains(".")) amount += ".00";
            amount += ek;
            return amount;
        }
        public static string ToFormatDateForResponse(this string date)
        {
            //17/10/2019 17:07
            var year = date.Substring(6, 4);
            var month = date.Substring(3, 2);
            var day = date.Substring(0, 2);
            return year + month + day;
        }
        public static string ToFormatDateWithHhForResponse(this string date, string time)
        {
            var year = date.Substring(6, 4);
            var month = date.Substring(3, 2);
            var day = date.Substring(0, 2);
            var hour = time.Substring(0, 2);
            var minute = time.Substring(3, 2);
            var second = time.Substring(6, 2);
            return year + month + day + hour + minute + second;
        }
        public static string ToFormatDate(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return year + "-" + month + "-" + day;
        }

        public static string ToFormatDate2(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(5, 2);
            var day = date.Substring(8, 2);
            return year + "-" + month + "-" + day;
        }
        public static string ToFormatDate3(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(5, 2);
            var day = date.Substring(8, 2);
            return year +  month +  day;
        }

        public static string ToFormatTime(this string time)
        {
            var hour = time.Substring(0, 2);
            var minute = time.Substring(2, 2);
            var seconds = time.Substring(4, 2);
            return hour + "." + minute + "." + seconds + ".000001";
        }
        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", ".").Replace("-", "").Replace("+", "");
            return amount;
        }
    }
}
