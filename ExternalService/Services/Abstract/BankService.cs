﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using ExternalService.Model;
using ExternalServices.Model;

namespace ExternalServices.Services.Abstract
{

    public abstract class BankService
    {
        protected bool isServiceUp;
        public abstract ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request);
        public bool CheckServiceAvailability(string url)
        {
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Timeout = 5000;
                var response = (HttpWebResponse)myRequest.GetResponse();

                return response.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //  not available at all, for some reason
                Debug.Write(string.Format("{0} unavailable: {1}", url, ex.Message));
                return false;
            }
        }
        protected X509Certificate2 GetCertificate(string cer)
        {
            X509Certificate2 cert;
            var path = AppDomain.CurrentDomain.BaseDirectory;
            cert = new X509Certificate2(path + cer);
            return cert;
        }
       
    }
}
