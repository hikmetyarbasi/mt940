﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using ExternalService.Helpers;
using ExternalService.Model;
using ExternalService.Model.IsBank;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Microsoft.Extensions.Configuration;
using MT940.api.Helpers;
using Newtonsoft.Json;
using tr.com.yapikredi.api;

namespace ExternalServices.Services.Concrete
{
    public class YapikrediService : BankService
    {
        private EhoAccountTransactionServiceClient _client;
        private IEhoAccountTransactionService serviceProxy;
        IConfiguration _configuration;
        ICustomHttpClient client;

        public IsBankaUser _user
        {
            get;
            set;
        }

        public YapikrediService(IConfiguration configuration, IEnumerable<Claim> user)
        {
            _configuration = configuration;
            client = new CustomHttpClient(configuration.GetSection("AppSettings").Get<AppSettings>().YkbHostUrl);
            _user = LoadUserInfo(user);
        }

        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    MusteriNo = enumerable?.SingleOrDefault(p => p.Type == "MusteriNo")?.Value,
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
         
            var response = new ApiResponse<ResponseObject>();
            try
            {
                request.RequestedObject.Param2 = _user.UserDetail.UserName;
                request.RequestedObject.Param3 = _user.UserDetail.Password;
                response = client.PostRequest<ApiRequest<RequestObject>, ApiResponse<ResponseObject>>("ykb/send",request).Result;
                if (string.IsNullOrEmpty(response.StatusCode))
                {
                    response.StatusCode = "0";
                    response.StatusMessage = "Successful.";
                }
                return response;
            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = e.Message + "\r InnerException : " + JsonConvert.SerializeObject(e);
            }

            return response;
        }

        public sorgulaResponse1 CreateDummyData()
        {
            return new sorgulaResponse1
            {
                @return = new responseEhoAccountTransaction()
                {
                    bankaAdi = "Yapı ve Kredi Bankası A.Ş.",
                    bankaKodu = "067",
                    bankaVergiDairesi = "Büyük Mükellefler Vergi Dairesi",
                    bankaVergiNumarasi = "9370020892",
                    hataAciklamasi = "",
                    hataKodu = "",
                    id = 206558828,
                    hesaplar = new ehoAccountTransactionHesaplarDTO()
                    {
                        hesap = new[]
                        {
                            new ehoAccountTransactionHesapDTO
                            {
                                acilisBakiyesi = "89528.86",
                                bekleyenIslemTutarı = 0,
                                bekleyenIslemTutarıSpecified = false,
                                dovizTipi = "TL",
                                hesapNo = "85221885",
                                kapanisBakiyesi = "217753.84",
                                sonBakiye = "",
                                subeAdi = "ANADOLUYAKASI KURUMSAL BANKACILIK MR/İSTANBUL",
                                subeKodu = "673",
                                hareketler = new ehoAccountTransactionHareketlerDTO()
                                {
                                    hareket = new[]
                                    {
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "1",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "COC",
                                            valor = "191001",
                                            dekontNo = "602458320548",
                                            siraNo = "2",
                                            anlikBakiye = "97955.26",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞSTKOM",
                                            borcAlacak = "B",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "138.77",
                                            hareketKey = "19100107501475150733",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "COC",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "3",
                                            anlikBakiye = "97948.32",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞSTBSMV",
                                            borcAlacak = "B",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "6.94",
                                            hareketKey = "19100107501479150734",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "4",
                                            anlikBakiye = "97926.68",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞK",
                                            borcAlacak = "B",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "5",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "6",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "7",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "8",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "9",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "10",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "11",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "12",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "13",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "14",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "15",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        },
                                        new ehoAccountTransactionHareketDTO
                                        {
                                            karsiHesapVNo = "1250356459",
                                            islemTipi = "CCP",
                                            valor = "191002",
                                            dekontNo = "602458320548",
                                            siraNo = "16",
                                            anlikBakiye = "98094.03",
                                            muhasebeTarihi = "191001",
                                            aciklama = "1892702380X011019300919PEŞİNSATIŞ",
                                            borcAlacak = "A",
                                            fizikselIslemTarihi = "191001",
                                            gonderenIbanNo = "TR930006701000000049287228",
                                            tutar = "8565.17",
                                            hareketKey = "19100107501468150732",
                                            alacakliVKN = "1250356459",
                                            bilgi = "",
                                            borcluVKN = "1250356459",
                                            gonderenAd = "AYTEMİZ AKARYAKIT DAĞITIM ANONİM ŞİR",
                                            gonderenBanka = "0067",
                                            gonderenSube = "0673",
                                            hashGonderenIbanNo = "",
                                            hashTckn = "",
                                            karsiBankaSorguNo = "",
                                            kontratNo = "602458320548",
                                            saat = "07501468",
                                            sorguNo = "",
                                            uzunAciklama = ""
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            };
        }


    }
}
