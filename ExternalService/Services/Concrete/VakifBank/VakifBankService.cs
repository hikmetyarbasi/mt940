﻿using ExternalService.Helpers;
using ExternalService.Model;
using ExternalService.Model.Finansbank;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalService.Services.Concrete.VakifBank.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using log4net;
using LoggerService;
using Microsoft.Extensions.Configuration;
using MT940.api.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using tr.com.teb.api;
using tr.com.vakifbank.api;

namespace ExternalService.Services.Concrete.VakifBank
{
    public class VakifBankService : BankService
    {
        SOnlineEkstreServisClient _client = null;

        private ILoggerManager _logger;
        public IsBankaUser _user
        {
            get;
            set;
        }
        public VakifBankService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            _logger = new LoggerManager();
            var url = "https://vbservice.vakifbank.com.tr/HesapHareketleri.OnlineEkstre/SOnlineEkstreServis.svc";
            //this.isServiceUp = CheckServiceAvailability(url);
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.OpenTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;

            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new SOnlineEkstreServisClient(binding, endpoint);
            _client.Endpoint.EndpointBehaviors.Add(logbehavior);
            _client.ClientCredentials.UserName.UserName = "username";
            _client.ClientCredentials.UserName.Password = "password";
        }

        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    MusteriNo = enumerable?.SingleOrDefault(p => p.Type == "MusteriNo")?.Value,
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }


        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            try
            {
                var req = new DtoEkstreSorgu
                {
                    MusteriNo = _user.UserDetail.MusteriNo,
                    KurumKullanici = _user.UserDetail.UserName,
                    Sifre = _user.UserDetail.Password,
                    SorguBaslangicTarihi = request.RequestedObject.StartDate.toRequestDate(request.RequestedObject.StartTime),
                    SorguBitisTarihi = request.RequestedObject.EndDate.toRequestDate(request.RequestedObject.EndTime),
                    HesapNo = request.RequestedObject.AccountNumber
                };
                _logger.LogInfo(JsonConvert.SerializeObject(req));

                var resp = _client.GetirHareketAsync(req).Result;

                response = new ApiResponse<ResponseObject>
                {
                    ResponseObject = new ResponseObject
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = resp.Hesaplar.ToMapAccount()
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                response.StatusCode = "1";
                response.StatusMessage = e.Message + "\r InnerException : " + JsonConvert.SerializeObject(e);
            }
            return response;
        }
    }
}
