﻿using System.Collections.Generic;
using ExternalService.Model;
using ExternalService.Services.Abstract;
using ExternalServices.Model;
using tr.com.garanti.api;
using tr.com.yapikredi.api;

namespace ExternalService.Services.Concrete.Garanti.Utils
{
    public  static class GbExtMapMethods 
    {
        public static List<Hesap> ToMapAccount(this FirmAccountDetail[] accounts)
        {
            var list = new List<Hesap>();

            foreach (var account in accounts)
            {
                var hesap = new Hesap()
                {
                    Hareketler = account.AccountActivities?.ToMapHareket(),
                    Iban = account.IBAN,
                    Bakiye = account.Balance.ToMapAmount(),
                    HesapAcilisTarih = account.OpenDate.ToFormatDateForResponse(),
                    SonHareketTarihi = account.LastActivityDate.ToFormatDateForResponse(),
                    IslemiYapanSube = account.BranchNum,
                    Blokemeblag = account.BlockedAmount.ToMapAmount(),
                    KullanilabilirBa = account.AvailableBalance.ToMapAmount(),
                    MusteriNo = account.AccountNum,
                    AcilisIlkBakiye = "",
                    DovizTipi = "",
                    HesapNo = "",
                    KapanisBakiyesi = "",
                    SubeAdi = "",
                    SubeKodu = "",
                };


                list.Add(hesap);
            }

            return list;
        }

        public static List<Hareket> ToMapHareket(this AccountActivityDetail[] activities)
        {
            var list = new List<Hareket>();
            foreach (var activity in activities)
            {
                var hareket = new Hareket
                {
                    Tutar = activity.Amount.ToMapAmount(),
                    Shkzg = activity.Amount.ToMapShkzg(),
                    Satir86 = activity.Explanation.Replace("  "," "),
                    LastTmSt = activity.TransactionReferenceId,
                    KarsiHesapIban = activity.CorrIBAN,
                    Aciklama = activity.Explanation.Replace("  ", " "),
                    Vkn = activity.CorrVKN,
                    TutarBorcAlacak = activity.Amount.ToMapTutarBorcAlacak(),
                    FonksiyonKodu = activity.TransactionId,
                    Mt940FonksiyonKo = activity.TransactionId,
                    FisNo = activity.TransactionReferenceId,
                    AmirVkn = activity.CorrVKN,
                    AmirTckn = activity.CorrVKN,
                    AcilisGunBakiye = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    BorcluIban = "",
                    CariBakiye = activity.Balance.ToMapAmountCariBakiye(),
                    DigerMusteriNo = "",
                    Ekbilgi = "",
                    FaizOrani = "",
                    GondAdi = "",
                    GondHesapNo = "",
                    GondSubeKodu = "",
                    HareketKey = "",
                    Hata = "",
                    HavaleRefNo = "",
                    HesapAdi = "",
                    HesapCinsi = "",
                    HesapTuru = "",
                    HesapTuruKod = "",
                    IslemAdi = "",
                    IslemKanali = "",
                    IslemKodu = "",
                    IslemOncesiBakiye = "",
                    IslemTarihi = activity.ActivityDate.ToFormatDateForResponse(),
                    KapanisBakiye = "",
                    KrediLimit = "",
                    KrediliKulBak = "",
                    MuhasebeTarih = "",
                    ReferenceNo = "",
                    SiraNo = "",
                    SonBakiye = "",
                    SonBakiyeBorcAla = "",
                    Urf = "",
                    VadeTarihi = "",
                    ValorTarihi = "",
                };
                list.Add(hareket);
            }

            return list;
        }

        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", "").Replace("-", "").Replace("+","") ;
            if (!amount.Contains('.')) amount += ".00";
            return amount;
        }
        public static string ToMapAmountCariBakiye(this string amount)
        {
            var ek = amount.Trim().StartsWith("-") ? "-" : "";
            amount = amount.Replace(",", "");
            if (!amount.Contains(".")) amount += ".00";
            amount += ek;
            return amount;
        }
        public static string ToMapShkzg(this string amount)
        {
            return amount.Trim().StartsWith('-') == true ? "S" : "H";
        }
        public static string ToMapTutarBorcAlacak(this string amount)
        {
            return amount.Trim().StartsWith('-') == true ? "-" : "+";
        }
        

    }

    
}
