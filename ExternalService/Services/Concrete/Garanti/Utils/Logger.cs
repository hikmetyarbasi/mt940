﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExternalService.Services.Concrete.Garanti.Utils
{
    public class Logger
    {
        public static void Log(string log)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(log);
            var logfile = "log//" + DateTime.Now.ToString("yyyyMMddThhmmss") + ".txt";
            File.AppendAllText(logfile, sb.ToString());
            sb.Clear();
        }
    }
}
