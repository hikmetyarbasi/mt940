﻿using ExternalService.Model;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using ExternalService.Helpers;
using Newtonsoft.Json;
using tr.com.halkbankasi.api;
using Microsoft.Extensions.Configuration;
using MT940.api.Helpers;

namespace ExternalService.Services.Concrete.HalkBankasi
{
    public class HalkBankService : BankService
    {
        ICustomHttpClient _client = null;
        public HalkBankService(IConfiguration configuration)
        {
            _client = new CustomHttpClient(configuration.GetSection("AppSettings").Get<AppSettings>().YkbHostUrl);
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            try
            {
                response = _client.PostRequest<ApiRequest<RequestObject>, ApiResponse<ResponseObject>>("halkbank/send",request).Result;
                if (string.IsNullOrEmpty(response.StatusCode))
                {
                    response.StatusCode = "0";
                    response.StatusMessage = "Successful.";
                }
                return response;
            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = e.Message + "\r InnerException : " + JsonConvert.SerializeObject(e);
            }

            return response;
        }
    }
}
