﻿using ExternalService.Model;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalService.Services.Concrete.Albaraka.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.ServiceModel;
using System.Text;
using tr.com.albaraka.api;

namespace ExternalService.Services.Concrete.Albaraka
{
    public class AlbarakaService : BankService
    {
        public HesapBilgileriServiceSoapClient _client;


        public IsBankaUser _user
        {
            get;
            set;
        }
        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }
        public AlbarakaService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            var url = "https://eservice.albarakaturk.com.tr:10214/invoiceincomingsite/HesapBilgileriService.asmx";
            //this.isServiceUp = CheckServiceAvailability(url);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.OpenTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new HesapBilgileriServiceSoapClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\albaraka.cer");


            _client.ClientCredentials.UserName.UserName = "";
            _client.ClientCredentials.UserName.Password = "";
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();

            try
            {
                var respHesap = _client.getHesapDetaylariAsync(_user.UserDetail.UserName,
                                                               _user.UserDetail.Password,
                                                               new hesapDetaylariInput
                                                               {
                                                                   musteriNo = _user.UserDetail.UserName
                                                               }).Result.responseData.hesapDetaylari?.Where(x=>x.hesapNo == request.RequestedObject.AccountNumber).FirstOrDefault<hesap>();

                var respHareketler = _client.getHesapHareketleriAsync(_user.UserDetail.UserName,
                                                            _user.UserDetail.Password, new hesapHareketleriInput
                                                            {
                                                                hesapNo = request.RequestedObject.AccountNumber,
                                                                musteriNo = _user.UserDetail.UserName,
                                                                basTarih = request.RequestedObject.StartDate,
                                                                sonTarih = request.RequestedObject.EndDate
                                                            }
                                                            ).Result.responseData.hesapHareketleri?.Where(x=>x.hesapNo == request.RequestedObject.AccountNumber).FirstOrDefault<hesapHareketleriHesap>();

                response = new ApiResponse<ResponseObject>()
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = respHesap?.toMapHesap(respHareketler)
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };

            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = JsonConvert.SerializeObject(e);
            }
            return response;

        }
    }
}
