﻿using System;
using System.Collections.Generic;
using System.Text;
using tr.com.albaraka.api;
using Hareket = ExternalService.Model.Hareket;
using Hesap = ExternalService.Model.Hesap;

namespace ExternalService.Services.Concrete.Albaraka.Utils
{
    public static class AlbarakaExtMapMethods
    {
        public static int toNumber(this string value) {
            return Convert.ToInt32(value);
        }
        public static string toFormatDateForRequest(this string date)
        {
            //20200720
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return day + month +year ;
        }
        public static string ToFormatDateForResponse(this string date)
        {
            //2020-07-20
            var year = date.Substring(0, 4);
            var month = date.Substring(5, 2);
            var day = date.Substring(8, 2);
            return year + month + day;
        }
        public static string ToManipulateShkzg(this string amount)
        {
            switch (amount)
            {
                case "A":
                    return "H";
                case "B":
                    return "S";
                default:
                    return "";
            }
        }
        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", ".").Replace("-", "").Replace("+", "");
            return amount;
        }
        public static List<Hesap> toMapHesap(this hesap hesap, hesapHareketleriHesap hareket)
        {
            var list = new List<Hesap>();
          
                var item = new Hesap()
                {
                    Hareketler = hareket.hesapHareket.ToMapHareket(),
                    Iban = hesap.iban,
                    Bakiye = hesap.toplamBakiye.ToString().ToMapAmount(),
                    HesapAcilisTarih = "",
                    SonHareketTarihi = "",
                    IslemiYapanSube = hesap.sube,
                    Blokemeblag = hesap.blokeBakiye.ToMapAmount(),
                    KullanilabilirBa = hesap.kullanabilirBakiye.ToMapAmount(),
                    MusteriNo = hesap.hesapNo,
                    AcilisIlkBakiye = "",
                    DovizTipi = "",
                    HesapNo = hesap.hesapNo,
                    KapanisBakiyesi = "",
                    SubeAdi = "",
                    SubeKodu = ""
                };
            list.Add(item);
                       
            return list;
        }
        public static List<Hareket> ToMapHareket(this hesapHareket[] detaylar)
        {
            var list = new List<Hareket>();
            foreach (var hareket in detaylar)
            {
                list.Add(new Hareket()
                {
                    Tutar = hareket.islemTutari.ToMapAmount(),
                    Shkzg = hareket.borcAlacak.ToManipulateShkzg(),
                    Satir86 = hareket.aciklama,
                    CariBakiye = hareket.islemsonrasibakiye.Trim().ToMapAmount(),
                    ValorTarihi = hareket.tarih,
                    IslemTarihi = hareket.tarih,
                    KarsiHesapIban = hareket.karsiHesapIban.Trim(),
                    Aciklama = hareket.aciklama,
                    TutarBorcAlacak = hareket.borcAlacak.Trim() == "B" ? "-" : "+",
                    FonksiyonKodu = hareket.Code,
                    Mt940FonksiyonKo = hareket.Code,
                    FisNo = hareket.fisNo,
                    AmirVkn = "",
                    AmirTckn = "",
                    HareketKey = hareket.tarih + hareket.saat + hareket.SeqNumber,
                    MuhasebeTarih = "",
                    SiraNo = hareket.SeqNumber,
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.MuhRefNo,
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.Code,
                    HesapCinsi = "",
                    SonBakiye = hareket.islemsonrasibakiye.Trim().ToMapAmount(),
                    IslemKanali = hareket.ChannelCode,
                    IslemOncesiBakiye = hareket.islemsonrasibakiye.Trim().ToMapAmount(),
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = hareket.MuhRefNo,
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.tarih + hareket.saat + hareket.SeqNumber,
                    Vkn = ""
                });
            }
            return list;
        }
    }
}
