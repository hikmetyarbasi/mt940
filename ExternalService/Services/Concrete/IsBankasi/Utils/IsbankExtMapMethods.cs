﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalService.Model.IsBank;
using ExternalService.Services.Abstract;

namespace ExternalService.Services.Concrete.IsBankasi.Utils
{
    public static class IsbankExtMapMethods
    {
        public static List<Model.Hesap> ToMapAccount(this List<Hesap> hesaplar, string startdate, string enddate)
        {
            var list = new List<Model.Hesap>();
            foreach (var hesap in hesaplar)
            {
                var item = new Model.Hesap()
                {
                    Hareketler = hesap.Hareketler?.ToMapHareket(startdate, enddate),
                    Iban = hesap.Tanimlamalar.IbanNo,
                    Bakiye = hesap.Tanimlamalar.Bakiye?.ToMapAmount().Trim(),
                    HesapAcilisTarih = hesap.Tanimlamalar.HesapAcilisTarihi?.ToFormatDateForResponse2(),
                    SonHareketTarihi = hesap.Tanimlamalar.SonHareketTarihi?.ToFormatDateForResponse2(),
                    IslemiYapanSube = hesap.Tanimlamalar.SubeKodu,
                    Blokemeblag = hesap.Tanimlamalar.KrediLimiti?.ToMapAmount().Trim(),
                    KullanilabilirBa = hesap.Tanimlamalar.Bakiye?.Trim(),
                    MusteriNo = hesap.Tanimlamalar.MusteriNo,
                    AcilisIlkBakiye = "",
                    DovizTipi = "",
                    HesapNo = hesap.Tanimlamalar.HesapNo,
                    KapanisBakiyesi = "",
                    SubeAdi = "",
                    SubeKodu = "",
                };

                list.Add(item);
            }

            return list;
        }

        public static List<Model.Hareket> ToMapHareket(this List<Hareket> hareketler, string startdate, string enddate)
        {
            var list = new List<Model.Hareket>();
            foreach (var hareket in hareketler)
            {
                if (Convert.ToDateTime(hareket.timeStamp.ToFormatDate2()) >= Convert.ToDateTime(startdate) &&
               Convert.ToDateTime(hareket.timeStamp.ToFormatDate2()) <= Convert.ToDateTime(enddate))
                {
                    list.Add(new Model.Hareket()
                    {
                        Tutar = hareket.Miktar?.Trim().ToMapAmount(),
                        Shkzg = hareket.Miktar?.Trim().Substring(0, 1).ToManipulateShkzg(),
                        Satir86 = hareket.Aciklama,
                        CariBakiye = hareket.Bakiye?.Trim().ToMapAmountCariBakiye(),
                        ValorTarihi = "20" + hareket.valor,
                        IslemTarihi = hareket.timeStamp?.ToFormatDateForResponse(),
                        KarsiHesapIban = "",
                        Aciklama = hareket.Aciklama,
                        TutarBorcAlacak = hareket.Miktar.Trim().StartsWith('-') ? "-" : "+",
                        FonksiyonKodu = hareket.IslemTuru,
                        Mt940FonksiyonKo = hareket.IslemTuru,
                        FisNo = hareket.HareketSirano,
                        AmirVkn = hareket.KarsiHesapVKN,
                        AmirTckn = hareket.KarsiHesapVKN,
                        HareketKey = hareket.DekontNo,
                        MuhasebeTarih = "",
                        SiraNo = hareket.IslemTod,
                        KrediliKulBak = "",
                        KrediLimit = "",
                        ReferenceNo = hareket.IslemTod,
                        GondAdi = hareket.Kaynak,
                        BorcluIban = "",
                        GondHesapNo = hareket.KarsiHesap,
                        Urf = "",
                        IslemAdi = "",
                        IslemKodu = "",
                        HesapCinsi = "",
                        SonBakiye = "",
                        IslemKanali = "",
                        IslemOncesiBakiye = "",
                        HesapAdi = "",
                        AcilisGunBakiye = "",
                        KapanisBakiye = "",
                        Hata = "",
                        HesapTuru = "",
                        SonBakiyeBorcAla = "",
                        FaizOrani = "",
                        AlacakliIban = "",
                        AliciHesapNo = "",
                        HavaleRefNo = "",
                        VadeTarihi = "",
                        DigerMusteriNo = "",
                        GondSubeKodu = "",
                        HesapTuruKod = "",
                        Ekbilgi = "",
                        LastTmSt = hareket.timeStamp,
                        Vkn = hareket.KarsiHesapVKN
                    });
                }
            }
            return list;
        }

        public static string ToManipulateShkzg(this string borcAlacak)
        {
            switch (borcAlacak)
            {
                case "-":
                    return "S";
                default:
                    return "H";
            }
        }
        public static string ToSDate(this string date)
        {
            if (date.Length == 8)
            {
                var year = date.Substring(0, 4);
                var month = date.Substring(4, 2);
                var day = date.Substring(6, 2);
                return day + "." + month + "." + year;
            }
            return "01.01.2000";
        }
        public static string ToSTime(this string time)
        {
            if (time.Length == 6)
            {
                var hour = time.Substring(0, 2);
                var minute = time.Substring(2, 2);
                var second = time.Substring(4, 2);
                return hour + ":" + minute + ":" + second;
            }
            return "00:00:00";
        }
        public static string ToFormatDateForResponse(this string date)
        {
            //2020-06-21-09:
            var year = date.Substring(0, 4);
            var month = date.Substring(5, 2);
            var day = date.Substring(8, 2);
            return year + month + day;
        }
        public static string ToFormatDateForResponse2(this string date)
        {
            //13/08/1992
            var year = date.Substring(6, 4);
            var month = date.Substring(3, 2);
            var day = date.Substring(0, 2);
            return year + month + day;
        }
    }
}
