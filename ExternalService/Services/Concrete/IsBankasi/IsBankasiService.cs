﻿using ExternalService.Model;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.IsBankasi.Utils;
using Hesap = ExternalService.Model.Hesap;
using ExternalService.Services.Abstract;

namespace ExternalService.Services.Concrete.IsBankasi
{


    public class IsBankasiService : BankService
    {
        public IsBankaUser _user
        {
            get;
            set;
        }
        public IsBankasiService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
        }

        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    MusteriNo = enumerable?.SingleOrDefault(p => p.Type == "MusteriNo")?.Value,
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }


        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> req)
        {
            string responseFromServer = "";
            try
            {
                var resp = new ApiResponse<ResponseObject>();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                WebRequest request = WebRequest.Create("https://posmatik2.isbank.com.tr/AuthenticateSpecific.aspx");
                request.Method = "POST";
                string postData = "uid=" + _user.UserDetail.UserName + "&pwd=" + _user.UserDetail.Password + "&BeginDate=" + req.RequestedObject.StartDate.ToSDate() + " " + req.RequestedObject.StartTime.ToSTime() + "&EndDate=" + req.RequestedObject.EndDate.ToSDate() + " " + req.RequestedObject.EndTime.ToSTime();
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
                request.ContentLength = byteArray.Length;
                request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream, Encoding.GetEncoding("ISO-8859-9"));

                responseFromServer = reader.ReadToEnd();
                IsBankApiResponse info;

                XmlSerializer serializer2 = new XmlSerializer(typeof(IsBankApiResponse));
                TextReader reader2 = new StringReader(responseFromServer);
                info = (IsBankApiResponse)serializer2.Deserialize(reader2);
                reader.Close();
                reader.Close();
                dataStream.Close();
                response.Close();

                return new ApiResponse<ResponseObject>()
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = info.bankaAdi,
                        BankaKodu = info.bankaKodu,
                        BankaVergiDairesi = info.bankaVergiDairesi,
                        BankaVergiNumarasi = info.bankaVergiNumarasi,
                        Hesaplar = info.Hesaplar.Where(x => x.Tanimlamalar.HesapNo.Contains(req.RequestedObject.AccountNumber)).ToList().ToMapAccount(req.RequestedObject.StartDate.ToFormatDate(), req.RequestedObject.EndDate.ToFormatDate())

                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + "\n\n Response :" + responseFromServer);
            }
        }
    }
}
