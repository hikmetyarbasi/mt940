﻿using ExternalService.Model;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalService.Services.Concrete.Ziraat.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.ServiceModel;
using System.Text;
using tr.com.ziraatbankasi.api;

namespace ExternalService.Services.Concrete.Ziraat
{
    public class ZiraatService : BankService
    {
        public HesapHareketleriSoapClient _client;


        public IsBankaUser _user
        {
            get;
            set;
        }
        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }
        public ZiraatService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            var url = "https://hesap.ziraatbank.com.tr/HEK_NKYWS/HesapHareketleri.asmx";
            //this.isServiceUp = CheckServiceAvailability(url);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.OpenTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new HesapHareketleriSoapClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\ziraat.cer");
            //_client.Endpoint.EndpointBehaviors.Add(logbehavior);

            _client.ClientCredentials.UserName.UserName = "";
            _client.ClientCredentials.UserName.Password = "";
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            var musterino = request.RequestedObject.AccountNumber.Split('-')[0].toNumber();
            var ekno = request.RequestedObject.AccountNumber.Split('-')[1].toNumber();

            try
            {

                var resp = _client.SorgulaHesapHareketAsync(musterino, ekno, 
                                                            request.RequestedObject.StartDate.toFormatDateForRequest(),
                                                            request.RequestedObject.EndDate.toFormatDateForRequest(),
                                                            _user.UserDetail.UserName,
                                                            _user.UserDetail.Password);

                response = new ApiResponse<ResponseObject>()
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = resp.Result.hataAck,
                        HataKodu = resp.Result.hataKodu,
                        Solid = 0,
                        Hesaplar = resp.Result.toMapHesap()
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };

            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = JsonConvert.SerializeObject(e);
            }
            return response;

        }
    }
}
