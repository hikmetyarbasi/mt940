﻿using System;
using System.Collections.Generic;
using System.Text;
using tr.com.ziraatbankasi.api;
using Hareket = ExternalService.Model.Hareket;
using Hesap = ExternalService.Model.Hesap;

namespace ExternalService.Services.Concrete.Ziraat.Utils
{
    public static class AlbarakaExtMapMethods
    {
        public static int toNumber(this string value) {
            return Convert.ToInt32(value);
        }
        public static string toFormatDateForRequest(this string date)
        {
            //20200720
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return day + month +year ;
        }
        public static string ToFormatDateForResponse(this string date)
        {
            //2020-07-20
            var year = date.Substring(0, 4);
            var month = date.Substring(5, 2);
            var day = date.Substring(8, 2);
            return year + month + day;
        }
        public static string ToManipulateShkzg(this string amount)
        {
            switch (amount)
            {
                case "A":
                    return "H";
                case "B":
                    return "S";
                default:
                    return "";
            }
        }
        public static List<Hesap> toMapHesap(this HesapHareketCevap hesap)
        {
            var list = new List<Hesap>();
          
                var item = new Hesap()
                {
                    Hareketler = hesap.hareketdetay?.ToMapHareket(),
                    Iban = "",
                    Bakiye = hesap.AcilisBakiye?.ToString(),
                    HesapAcilisTarih = "",
                    SonHareketTarihi = "",
                    IslemiYapanSube = hesap.subeAdi,
                    Blokemeblag = hesap.BlokeliBakiye,
                    KullanilabilirBa = hesap.AcilisBakiye,
                    MusteriNo = hesap.hesapNo,
                    AcilisIlkBakiye = hesap.AcilisBakiye,
                    DovizTipi = "",
                    HesapNo = hesap.hesapNo,
                    KapanisBakiyesi = hesap.CariBakiye,
                    SubeAdi = "",
                    SubeKodu = ""
                };
            list.Add(item);
                       
            return list;
        }
        public static List<Hareket> ToMapHareket(this HareketlerDetay[] detaylar)
        {
            var list = new List<Hareket>();
            foreach (var hareket in detaylar)
            {
                list.Add(new Hareket()
                {
                    Tutar = hareket.tutar,
                    Shkzg = hareket.borcAlacak.ToManipulateShkzg(),
                    Satir86 = hareket.aciklama,
                    CariBakiye = hareket.Bakiye.Trim(),
                    ValorTarihi = hareket.valorTarihi.ToString("yyyyMMdd"),
                    IslemTarihi = hareket.islemTarihi.ToString("yyyyMMdd"),
                    KarsiHesapIban = "",
                    Aciklama = hareket.aciklama,
                    TutarBorcAlacak = hareket.borcAlacak.Trim() == "B" ? "-" : "+",
                    FonksiyonKodu = "",
                    Mt940FonksiyonKo = "",
                    FisNo = hareket.dekontNo,
                    AmirVkn = "",
                    AmirTckn = "",
                    HareketKey = hareket.timeStamp,
                    MuhasebeTarih = "",
                    SiraNo = "",
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.muhref,
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.islemTipi,
                    HesapCinsi = "",
                    SonBakiye = hareket.Bakiye.Trim(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.islemTarihi.ToString("yyyyMMddHHmmssfff"),
                    Vkn = hareket.tcknVkn
                });
            }
            return list;
        }
    }
}
