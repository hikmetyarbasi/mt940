﻿using ExternalService.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml;

namespace ExternalService.Services.Concrete.Hsbc.Utils
{
    public static class HsbcExtMapMethods
    {
        public static List<Model.Hesap> ToMapAccount(this XmlNodeList hesaplar, string startdate, string enddate, string accountNumber)
        {
            var list = new List<Model.Hesap>();
            foreach (XmlNode hesap in hesaplar)
            {
                if (hesap["Tanimlamalar"].ChildNodes.Item(2).InnerText.Contains(accountNumber.Replace("-", "")))
                {
                    XmlNodeList hareketler = hesap.SelectNodes("Hareketler/Hareket");
                    var item = new Model.Hesap()
                    {
                        Hareketler = hareketler.ToMapHareket(hesap),
                        Iban = hesap["Tanimlamalar"].ChildNodes.Item(2).InnerText,
                        Bakiye = hesap["Tanimlamalar"].ChildNodes.Item(9).InnerText,
                        HesapAcilisTarih = hesap["Tanimlamalar"].ChildNodes.Item(7).InnerText.ToFormatDateForResponse(),
                        SonHareketTarihi = hesap["Tanimlamalar"].ChildNodes.Item(8).InnerText.ToFormatDateForResponse(),
                        HesapNo = hesap["Tanimlamalar"].ChildNodes.Item(2).InnerText,
                        SubeAdi = hesap["Tanimlamalar"].ChildNodes.Item(5).InnerText,
                        SubeKodu = hesap["Tanimlamalar"].ChildNodes.Item(4).InnerText,
                        Blokemeblag = hesap["Tanimlamalar"].ChildNodes.Item(10).InnerText,
                        KullanilabilirBa = hesap["Tanimlamalar"].ChildNodes.Item(11).InnerText,
                        DovizTipi = hesap["Tanimlamalar"].ChildNodes.Item(6).InnerText,
                        MusteriNo = hesap["Tanimlamalar"].ChildNodes.Item(3).InnerText
                    };

                    list.Add(item);
                }
            }

            return list;
        }

        public static List<Model.Hareket> ToMapHareket(this XmlNodeList hareketler, XmlNode hesap)
        {
            var list = new List<Model.Hareket>();
            var counter = 0;
            foreach (XmlNode hareket in hareketler)
            {
                counter++;
                list.Add(new Model.Hareket()
                {
                    Tutar = hareket["Miktar"].InnerText.Trim().ToMapAmount(),
                    Shkzg = hareket["Miktar"].InnerText.Trim().Substring(0, 1).ToManipulateShkzg(),
                    Satir86 = hareket["Aciklama"].InnerText,
                    CariBakiye = "",
                    ValorTarihi = hareket["Tarih"].InnerText.ToFormatDateForResponse(),
                    IslemTarihi = hareket["Tarih"].InnerText.ToFormatDateForResponse(),
                    KarsiHesapIban = "",
                    Aciklama = hareket["Aciklama"].InnerText,
                    TutarBorcAlacak = hareket["Miktar"].InnerText.Trim().StartsWith('-') ? "-" : "+",
                    FonksiyonKodu = hareket["IslemKodu"].InnerText,
                    Mt940FonksiyonKo = hareket["IslemKodu"].InnerText,
                    FisNo = hareket["DekontNo"].InnerText,
                    AmirVkn = hareket["TCKN_VKN"].InnerText,
                    AmirTckn = hareket["TCKN_VKN"].InnerText,
                    HareketKey = hareket["DekontNo"].InnerText,
                    MuhasebeTarih = "",
                    SiraNo = counter.ToString(),
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = "",
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = hareket["GonderenHesap"].InnerText,
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket["IslemKodu"].InnerText,
                    HesapCinsi = "",
                    SonBakiye = hesap.SelectNodes("Tanimlamalar").Item(0)["KullanilabilirBakiye"].InnerText,
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = hesap.SelectNodes("Tanimlamalar").Item(0)["HesapAdi"].InnerText,
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = hesap.SelectNodes("Tanimlamalar").Item(0)["HesapTuru"].InnerText,
                    SonBakiyeBorcAla = "",
                    FaizOrani = hesap.SelectNodes("Tanimlamalar").Item(0)["FaizOrani"].InnerText,
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = hesap.SelectNodes("Tanimlamalar").Item(0)["VadeTarihi"].InnerText.ToFormatDateForResponse(),
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = DateTime.ParseExact(hareket["Tarih"].InnerText, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString("yyyyMMddHHmm") + counter,
                    Vkn = hareket["TCKN_VKN"].InnerText
                });
            }
            return list;
        }
        public static string ToFormatDateForResponse(this string date)
        {
            //16/03/2022 02:34
            var day = date.Substring(0, 2);
            var month = date.Substring(3, 2);
            var year = date.Substring(6, 4);
            return year + month + day;
        }
        public static string toFormatDateForLastTmSt(this string date)
        {
            //16/03/2022 02:34
            var year = date.Substring(0, 2);
            var month = date.Substring(3, 2);
            var day = date.Substring(6, 4);
            var hour = date.Substring(11, 2);
            var min = date.Substring(14, 2);
            return year + month + day + hour + min;
        }
        public static string ToManipulateShkzg(this string borcAlacak)
        {
            switch (borcAlacak)
            {
                case "-":
                    return "S";
                default:
                    return "H";
            }
        }
    }
}
