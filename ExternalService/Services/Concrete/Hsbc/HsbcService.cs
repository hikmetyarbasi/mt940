﻿using ExternalService.Model;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalService.Services.Concrete.Hsbc.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.ServiceModel;
using System.Xml;
using tr.com.hsbc.api;


namespace ExternalService.Services.Concrete.Hsbc
{
    public class HsbcService : BankService
    {
        private AccountServiceSoapClient _client = null;

        public IsBankaUser _user
        {
            get;
            set;
        }

        public HsbcService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            var url = "https://internet.hsbc.com.tr/AccountWebservice/AccountService.asmx";
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new AccountServiceSoapClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\hsbc.cer");


            _client.ClientCredentials.UserName.UserName = _user.UserDetail.UserName;
            _client.ClientCredentials.UserName.Password = _user.UserDetail.Password;
        }
        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    MusteriNo = enumerable?.SingleOrDefault(p => p.Type == "AssociationCode")?.Value,
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }

        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            try
            {
                var resp = _client.AccountReportAsync(_user.UserDetail.MusteriNo, _user.UserDetail.UserName, _user.UserDetail.Password, DateTime.ParseExact(request.RequestedObject.StartDate, "yyyyMMdd", CultureInfo.InvariantCulture), DateTime.ParseExact(request.RequestedObject.EndDate, "yyyyMMdd", CultureInfo.InvariantCulture)).Result;
                XmlNodeList hesaplar = resp.SelectNodes("//Hesaplar/Hesap");
                return new ApiResponse<ResponseObject>()
                {
                    ResponseObject = new ResponseObject()
                    {
                        Hesaplar = hesaplar.ToMapAccount(request.RequestedObject.StartDate,request.RequestedObject.EndDate,request.RequestedObject.AccountNumber),
                        BankaAdi="",
                        BankaKodu = "",
                        BankaVergiDairesi="",
                        BankaVergiNumarasi="",
                        HataAciklamasi = "",
                        HataKodu="",
                        Solid=0
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return response;
        }
        public string GetPassword()
        {
            var resp = _client.GetNewPasswordAsync(_user.UserDetail.MusteriNo,_user.UserDetail.UserName, _user.UserDetail.Password);
            return resp.Result;
        }
    }
}
