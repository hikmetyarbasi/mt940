﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using ExternalService.Model;
using ExternalService.Services.Concrete.Finansbank.Utils;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using tr.com.finansbank.akpa.api;
using tr.com.halkbankasi.api;

namespace ExternalService.Services.Concrete.Finansbank
{
    public class FinansbankAkpaService : BankService
    {
        private AkpaDayanikliServicePortTypeClient _client = null;
        public FinansbankAkpaService()
        {
            var url = "https://fbmaestro.finansbank.com.tr/MaestroCoreEkstre/services/AkpaDayanikliService";
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new AkpaDayanikliServicePortTypeClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\finansbank.cer");
            //_client.Endpoint.EndpointBehaviors.Add(logbehavior);

            //_client.ClientCredentials.UserName.UserName = "AKPADAYANIKLIWS";
            //_client.ClientCredentials.UserName.Password = "AKPADAYANIKLIQNB1";
        }

        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            try
            {
                var resp = _client.getTransactionInfoAsync(new GetExtendedDetailTransactionInfo()
                {
                    userName = "AKPADAYANIKLIWS",
                    password = "AKPADAYANIKLIQNB1",
                    transactionInfoInputType = new GetExtendedDetailTransactionInfoInputType()
                    {
                        accountNo = request.RequestedObject.AccountNumber,
                        endDate = request.RequestedObject.EndDate.toFormatDate(request.RequestedObject.EndTime),
                        endDateSpecified = true,
                        iban = "",
                        startDate = request.RequestedObject.StartDate.toFormatDate(request.RequestedObject.StartTime),
                        startDateSpecified = true
                    }
                }).Result;
                if (resp?.@return?.errorCode == "EHS01" && resp?.@return?.errorDescription == "Hesap sorgulama başarılıdır")
                {
                    response = new ApiResponse<ResponseObject>()
                    {

                        ResponseObject = new ResponseObject()
                        {
                            BankaAdi = "",
                            BankaKodu = "",
                            BankaVergiDairesi = "",
                            BankaVergiNumarasi = "",
                            HataAciklamasi = resp?.@return?.errorDescription,
                            HataKodu = resp?.@return?.errorCode,
                            Solid = 0,
                            Hesaplar = resp.@return.accountInfoReturnType.toMapAccount()
                        },
                        StatusCode = "0",
                        StatusMessage = "Successful."
                    };
                }
                else
                {
                    response = new ApiResponse<ResponseObject>()
                    {

                        ResponseObject = new ResponseObject()
                        {
                            BankaAdi = "",
                            BankaKodu = "",
                            BankaVergiDairesi = "",
                            BankaVergiNumarasi = "",
                            HataAciklamasi = resp?.@return?.errorDescription,
                            HataKodu = resp?.@return?.errorCode,
                            Solid = 0,
                            Hesaplar = null
                        },
                        StatusCode = "0",
                        StatusMessage = "Successful."
                    };
                }
            }
            catch (System.Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return response;
        }
    }
}
