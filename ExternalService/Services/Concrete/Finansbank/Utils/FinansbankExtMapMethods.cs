﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalService.Model;
using ExternalService.Services.Abstract;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;

namespace ExternalService.Services.Concrete.Finansbank.Utils
{
    public static class FinansbankExtMapMethods
    {
        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", ".");
            return amount;
        }
        public static string ToFormatDateForResponse(this string date)
        {
           // 1990-03-22T00:00:00.000+02:00;
            var year = date.Substring(0, 4);
            var month = date.Substring(6, 2);
            var day = date.Substring(9, 2);
            return year + month + day;
        }
        public static DateTime? toFormatDate(this string date, string time)
        {
            return new DateTime(
                Convert.ToInt32(date.Substring(0, 4)),
                Convert.ToInt32(date.Substring(4, 2)),
                Convert.ToInt32(date.Substring(6, 2)),
                Convert.ToInt32(time.Substring(0,2)),
                Convert.ToInt32(time.Substring(2, 2)),
                Convert.ToInt32(time.Substring(4, 2))
            );
        }
        public static string ToManipulateShkzg(this string borcAlacak)
        {
            switch (borcAlacak)
            {
                case "A":
                    return "H";
                case "B":
                    return "S";
                default:
                    return "H";
            }
        }
        public static List<Hesap> toMapAccount(this tr.com.finansbank.akpa.api.ExtendedDetailAccountInfo[] hesaps)
        {

            var list = new List<Hesap>();
            foreach (var account in hesaps)
            {
                var item = new Hesap()
                {
                    Hareketler = account.transactions?.ToMapHareket(),
                    Iban = account.iban,
                    Bakiye = account.accountBalance?.ToString().ToMapAmount(),
                    HesapAcilisTarih = account.openingBalanceDate?.ToString("yyyyMMdd"),
                    SonHareketTarihi = account.lastTrxClosingProcessDate?.ToString("yyyyMMdd"),
                    IslemiYapanSube = account.branchCode,
                    Blokemeblag = account.accountBalance?.ToString().ToMapAmount(),
                    KullanilabilirBa = account.accountBalance.ToString().ToMapAmount(),
                    MusteriNo = account.accountNo,
                    AcilisIlkBakiye = account.openingBalance.ToString().ToMapAmount(),
                    DovizTipi = account.accountCurrencyCode,
                    HesapNo = account.accountNo,
                    KapanisBakiyesi = "",
                    SubeAdi = account.branchName,
                    SubeKodu = account.branchCode,
                };

                list.Add(item);
            };
            return list;

        }
        public static List<Hesap> toMapAccount(this tr.com.finansbank.aygazdg.api.ExtendedDetailAccountInfo[] hesaps)
        {

            var list = new List<Hesap>();
            foreach (var account in hesaps)
            {
                var item = new Hesap()
                {
                    Hareketler = account.transactions?.ToMapHareket(),
                    Iban = account.iban,
                    Bakiye = account.accountBalance?.ToString().ToMapAmount(),
                    HesapAcilisTarih = account.openingBalanceDate?.ToString("yyyyMMdd"),
                    SonHareketTarihi = account.lastTrxClosingProcessDate?.ToString("yyyyMMdd"),
                    IslemiYapanSube = account.branchName,
                    Blokemeblag = account.accountBalance?.ToString().ToMapAmount(),
                    KullanilabilirBa = account.accountBalance?.ToString().ToMapAmount(),
                    MusteriNo = account.customerNo,
                    AcilisIlkBakiye = "",
                    DovizTipi = "",
                    HesapNo = account.accountNo,
                    KapanisBakiyesi = "",
                    SubeAdi = account.branchName,
                    SubeKodu = account.branchCode,
                };

                list.Add(item);
            };
            return list;

        }
        public static List<Hesap> toMapAccount(this tr.com.finansbank.aygaz.api.ExtendedDetailAccountInfo[] hesaps)
        {

            var list = new List<Hesap>();
            foreach (var account in hesaps)
            {
                var item = new Hesap()
                {
                    Hareketler = account.transactions?.ToMapHareket(),
                    Iban = account.iban,
                    Bakiye = account.accountBalance?.ToString().ToMapAmount(),
                    HesapAcilisTarih = account.openingBalanceDate?.ToString("yyyyMMdd"),
                    SonHareketTarihi = account.lastTrxClosingProcessDate?.ToString("yyyyMMdd"),
                    IslemiYapanSube = account.branchCode,
                    Blokemeblag = account.accountBalance?.ToString().ToMapAmount(),
                    KullanilabilirBa = account.accountBalance?.ToString().ToMapAmount(),
                    MusteriNo = account.customerNo,
                    AcilisIlkBakiye = "",
                    DovizTipi = "",
                    HesapNo = account.accountNo,
                    KapanisBakiyesi = "",
                    SubeAdi = account.branchName,
                    SubeKodu = account.branchCode,
                };

                list.Add(item);
            };
            return list;

        }
        public static List<Hareket> ToMapHareket(this tr.com.finansbank.akpa.api.ExtendedDetailTransactionInfo[] harekets)
        {
            var list = new List<Hareket>();

            foreach (var hareket in harekets)
            {
                var item = new Hareket()
                {
                    Tutar = hareket.transactionAmount.ToString().ToMapAmount(),
                    Shkzg = hareket.debitOrCreditCode.ToString().ToManipulateShkzg().Trim(),
                    Satir86 = hareket.transactionDescription,
                    CariBakiye = hareket.transactionBalance?.ToString().ToMapAmount(),
                    ValorTarihi = "",
                    IslemTarihi = hareket.transactionDate?.ToString("yyyyMMdd"),
                    KarsiHesapIban = hareket.opponentIBAN,
                    Aciklama = hareket.transactionDescription,
                    TutarBorcAlacak = hareket.debitOrCreditCode.ToString().Trim().StartsWith('A') ? "+" : "-",
                    FonksiyonKodu = hareket.processCode,
                    Mt940FonksiyonKo = hareket.processCode,
                    FisNo = hareket.muhasebeFisNo,
                    AmirVkn = hareket.opponentTAXNoPIDNo,
                    AmirTckn = "",
                    HareketKey = hareket.transactionId.ToString(),
                    MuhasebeTarih = hareket.transactionDate?.ToString("yyyyMMdd"),
                    SiraNo = hareket.productOperationRefNo.ToString(),
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.productOperationRefNo.ToString(),
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.processCode,
                    HesapCinsi = "",
                    SonBakiye = hareket.transactionBalance?.ToString().ToMapAmount(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.transactionId.ToString(),
                    Vkn = ""
                };
                list.Add(item);
            }

            return list;
        }
        public static List<Hareket> ToMapHareket(this tr.com.finansbank.aygazdg.api.ExtendedDetailTransactionInfo[] harekets)
        {
            var list = new List<Hareket>();

            foreach (var hareket in harekets)
            {
                var item = new Hareket()
                {
                    Tutar = hareket.transactionAmount.ToString().ToMapAmount(),
                    Shkzg = hareket.debitOrCreditCode.ToString().ToManipulateShkzg().Trim(),
                    Satir86 = hareket.transactionDescription,
                    CariBakiye = hareket.transactionBalance?.ToString().ToMapAmount(),
                    ValorTarihi = "",
                    IslemTarihi = hareket.transactionDate?.ToString("yyyyMMdd"),
                    KarsiHesapIban = hareket.opponentIBAN,
                    Aciklama = hareket.transactionDescription,
                    TutarBorcAlacak = hareket.debitOrCreditCode.ToString().Trim().StartsWith('A') ? "+" : "-",
                    FonksiyonKodu = hareket.processCode,
                    Mt940FonksiyonKo = hareket.processCode,
                    FisNo = hareket.muhasebeFisNo,
                    AmirVkn = hareket.opponentTAXNoPIDNo,
                    AmirTckn = "",
                    HareketKey = hareket.transactionId.ToString(),
                    MuhasebeTarih = hareket.transactionDate?.ToString("yyyyMMdd"),
                    SiraNo = hareket.productOperationRefNo.ToString(),
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.productOperationRefNo.ToString(),
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.processCode,
                    HesapCinsi = "",
                    SonBakiye = hareket.transactionBalance?.ToString().ToMapAmount(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.transactionDate?.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK"),
                    Vkn = ""
                };
                list.Add(item);
            }

            return list;
        }
        public static List<Hareket> ToMapHareket(this tr.com.finansbank.aygaz.api.ExtendedDetailTransactionInfo[] harekets)
        {
            var list = new List<Hareket>();

            foreach (var hareket in harekets)
            {
                var item = new Hareket()
                {
                    Tutar = hareket.transactionAmount.ToString().ToMapAmount(),
                    Shkzg = hareket.debitOrCreditCode.ToString().ToManipulateShkzg().Trim(),
                    Satir86 = hareket.transactionDescription,
                    CariBakiye = hareket.transactionBalance?.ToString().ToMapAmount(),
                    ValorTarihi = "",
                    IslemTarihi = hareket.transactionDate?.ToString("yyyyMMdd"),
                    KarsiHesapIban = hareket.opponentIBAN,
                    Aciklama = hareket.transactionDescription,
                    TutarBorcAlacak = hareket.debitOrCreditCode.ToString().Trim().StartsWith('A') ? "+" : "-",
                    FonksiyonKodu = hareket.processCode,
                    Mt940FonksiyonKo = hareket.processCode,
                    FisNo = hareket.muhasebeFisNo,
                    AmirVkn = hareket.opponentTAXNoPIDNo,
                    AmirTckn = "",
                    HareketKey = hareket.transactionId.ToString(),
                    MuhasebeTarih = hareket.transactionDate?.ToString("yyyyMMdd"),
                    SiraNo = hareket.productOperationRefNo.ToString(),
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.productOperationRefNo.ToString(),
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.processCode,
                    HesapCinsi = "",
                    SonBakiye = hareket.transactionBalance?.ToString().ToMapAmount(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.transactionDate?.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK"),
                    Vkn = ""
                };
                list.Add(item);
            }

            return list;
        }
    }
}
