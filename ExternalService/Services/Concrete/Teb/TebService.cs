﻿using ExternalService.Model;
using ExternalService.Model.Teb;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalService.Services.Concrete.Teb.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Text;
using tr.com.teb.api;

namespace ExternalService.Services.Concrete.Teb
{
    public class TebService : BankService
    {
        TEBWebServicesClient _client = null;
        public TebService()
        {
            var url = "https://extws.teb.com.tr/heshar/HesHarSrv";
            //this.isServiceUp = CheckServiceAvailability(url);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.OpenTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new TEBWebServicesClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\teb.cer");
            _client.Endpoint.EndpointBehaviors.Add(logbehavior);

            _client.ClientCredentials.UserName.UserName = "WSHSHRGON";
            _client.ClientCredentials.UserName.Password = "H3s@pG0ster20150130";
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            var tebrequest = new RequestModel()
            {
                BASTAR = request.RequestedObject.StartDate.toRequestDate(),
                BITTAR = request.RequestedObject.EndDate.toRequestDate(),
                HESNO = request.RequestedObject.AccountNumber,
                FIRMA_AD = "aygaz",
                FIRMA_ANAHTAR = "aygaz$123",
                SUBENO = "538",
                GON_IBAN_EH = "E", 
                SON_BKY_EH = "E",
                DEKONTNO_EH = "E",
                BLOKE_BKY_EH ="E",
                ALICI_IBAN_EH = "E",
            };
            try
            {
                //File.WriteAllText("tebrequest.txt", tebrequest.WrapInCData());
                var resp = _client.TEBWebSrvAsync(
                    "WSHSHRGON",
                    "H3s@pG0ster20150130",
                    "874",
                    "P",
                    tebrequest.WrapInCData()
                    ).Result;

                var content =resp.outputDataXML?.Replace("<![CDATA[", "").Replace("]]>", "");
                var cdataobject = content?.mapCdataObject();

                response = new ApiResponse<ResponseObject>
                {
                    ResponseObject = new ResponseObject
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = resp.errorMsg,
                        HataKodu = resp.errorCode,
                        Solid = 0,
                        Hesaplar = cdataobject?.ToMapAccount()
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };
            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = e.Message + "\r InnerException : " + JsonConvert.SerializeObject(e);
            }
            return response;
        }
    }
}
