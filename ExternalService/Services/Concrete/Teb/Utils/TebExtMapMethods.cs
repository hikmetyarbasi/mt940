﻿using ExternalService.Model.Teb;
using ExternalService.Services.Abstract;
using ExternalServices.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ExternalService.Model;

namespace ExternalService.Services.Concrete.Teb.Utils
{
    public static class TebExtMapMethods
    {
        public static TebResponse mapCdataObject(this string cdataContent)
        {

            var item = Deserialize<TebResponse>(cdataContent);

            return item;
        }
        private static T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
        public static string ToManipulateShkzg(this string amount)
        {

            if (amount.StartsWith("-"))
            {
                return "S";
            }
            else
            {
                return "H";
            }
        }

        public static string toRequestDate(this string date)
        {

            return date.Substring(6, 2) + "." + date.Substring(4, 2) + "." + date.Substring(0, 4);
        }
        public static List<Hesap> ToMapAccount(this TebResponse account)
        {
            var list = new List<Hesap>();
            if (account is null)
                return list;
            var item = new Hesap()
            {
                Hareketler = account.Detaylar?.Detay?.ToMapHareket(),
                Iban = "",
                Bakiye = "",
                HesapAcilisTarih = "",
                SonHareketTarihi = "",
                IslemiYapanSube = account.Subeno,
                Blokemeblag = "",
                KullanilabilirBa = "",
                MusteriNo = "",
                AcilisIlkBakiye = "",
                DovizTipi = account.Detaylar?.Detay?.Length > 0 ? account.Detaylar?.Detay[0].Parakod : "",
                HesapNo = account.Hesno,
                KapanisBakiyesi = "",
                SubeAdi = "",
                SubeKodu = account.Subeno,
            };
            list.Add(item);
            return list;
        }

        public static List<Hareket> ToMapHareket(this Detay[] harekets)
        {
            var list = new List<Hareket>();

            foreach (var hareket in harekets)
            {
                list.Add(new Hareket
                {
                    Tutar = hareket.Tutar.Trim().ToMapAmount(),
                    Shkzg = hareket.Ba.Trim() == "A" ? "H" : "S",
                    Satir86 = hareket.Aciklama.Trim(),
                    CariBakiye = hareket.AnlikBky,
                    ValorTarihi = "",
                    IslemTarihi = (hareket.IslemTar + " " + hareket.IslemTarSaat).ToFormatDateForResponse(),
                    KarsiHesapIban = "",
                    Aciklama = hareket.Aciklama,
                    TutarBorcAlacak = hareket.Ba == "A" ? "+" : "-",
                    FonksiyonKodu = "",
                    Mt940FonksiyonKo = "",
                    FisNo = hareket.HareketKey,
                    AmirVkn = hareket.AlacakliVkn,
                    AmirTckn = "",
                    HareketKey = hareket.HareketKey,
                    MuhasebeTarih = hareket.IslemTar.ToFormatDateForResponse(),
                    SiraNo = "",
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.DekontNo,
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = hareket.HareketKey,
                    IslemKodu = hareket.IslemTur.ToString(),
                    HesapCinsi = "",
                    SonBakiye = "",
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = hareket.AliciIban,
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.IslemTar + " " + hareket.IslemTarSaat,
                    Vkn = ""
                });
            }

            return list;
        }
    }
}
