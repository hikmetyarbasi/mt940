﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;
using ExternalService.Model;
using ExternalService.Model.Akbank;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.Akbank.Utils;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Newtonsoft.Json;
using tr.com.akbank.api;
using tr.com.finansbank.aygaz.api;

namespace ExternalService.Services.Concrete.Akbank
{

    public class AkbankService : BankService
    {
        private ServiceSoapClient _client = null;

        public IsBankaUser _user
        {
            get;
            set;
        }

        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }
        public AkbankService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            var url = "https://firmahizmetleri.akbank.com/Extre_InterfaceService/Service.asmx";
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new ServiceSoapClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\akbank.cer");
            //_client.Endpoint.EndpointBehaviors.Add(logbehavior);
            _client.ClientCredentials.UserName.UserName = _user.UserDetail.UserName;
            _client.ClientCredentials.UserName.Password = _user.UserDetail.Password;

        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            try
            {
                var resp = _client.GetExtreWithParamsAsync(
                request.RequestedObject.Param1,
                request.RequestedObject.AccountNumber,
                "",
                "",
                request.RequestedObject.StartDate + request.RequestedObject.StartTime + "000000",
                request.RequestedObject.EndDate + request.RequestedObject.EndTime + "000000"
                ).Result;
                XmlSerializer serializer2 = new XmlSerializer(typeof(AkbankResponse));
                //string fileName = @"C:\Project\MT940\MT940\docs\akbank\akbankresponse.txt";
                //string XmlFirstRow = File.ReadAllText(fileName);

                TextReader reader = new StringReader("<Hesaps>" + resp.InnerXml.Replace("xmlns=\"http://akbank.extreservis.org/ExtreSchema.xsd\"", "") + "</Hesaps>");
                //TextReader reader = new StringReader(XmlFirstRow);
                AkbankResponse info = (AkbankResponse)serializer2.Deserialize(reader);
                reader.Close();
                response = new ApiResponse<ResponseObject>()
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = info.Hesaps.Where(x=>x.DovizKodu == request.RequestedObject.CurrencyCode).ToList().toMapHesap()
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };
            }
            catch (System.Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = JsonConvert.SerializeObject(e);
            }

            return response;
        }
    }
}
