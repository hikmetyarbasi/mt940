﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalService.Model;
using ExternalService.Model.Akbank;
using ExternalService.Services.Abstract;
using Hesap = ExternalService.Model.Akbank.Hesap;

namespace ExternalService.Services.Concrete.Akbank.Utils
{
    public static class AkbankExtMapMethods
    {
        public static string ToFormatDateForResponse(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return year + month + day;
        }

        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", ".").Replace("-","").Replace("+","");
            return amount;
        }

        public static string ToManipulateShkzg(this string amount)
        {
            switch (amount)
            {
                case "+":
                    return "H";
                case "-":
                    return "S";
                default:
                    return "";
            }
        }
        public static List<Model.Hesap> toMapHesap(this List<Hesap> hesaplar)
        {
            var list = new List<Model.Hesap>();
            foreach (var hesap in hesaplar)
            {
                var item = new Model.Hesap()
                {
                    Hareketler = hesap.Detay?.ToMapHareket(),
                    Iban = hesap.IBAN,
                    Bakiye = hesap.Bakiye?.ToMapAmount(),
                    HesapAcilisTarih = hesap.HesapAcilisTarihi?.ToFormatDateForResponse(),
                    SonHareketTarihi = hesap.SonHareketTarihi?.ToFormatDateForResponse(),
                    IslemiYapanSube = hesap.SubeKodu,
                    Blokemeblag = "",
                    KullanilabilirBa = hesap.Bakiye?.ToMapAmount(),
                    MusteriNo = "",
                    AcilisIlkBakiye = hesap.AcilisIlkBakiye.ToMapAmount(),
                    DovizTipi = hesap.DovizKodu,
                    HesapNo = "",
                    KapanisBakiyesi = hesap.CariBakiye.ToMapAmount(),
                    SubeAdi = "",
                    SubeKodu = ""

                };

                list.Add(item);
            }

            return list;
        }
        public static List<Hareket> ToMapHareket(this List<Detay> detaylar)
        {
            var list = new List<Hareket>();
            foreach (var hareket in detaylar)
            {
                list.Add(new Hareket()
                {
                    Tutar = hareket.Tutar.Trim().ToMapAmount(),
                    Shkzg = hareket.TutarBorcAlacak.Trim().ToManipulateShkzg(),
                    Satir86 = hareket.Aciklama,
                    CariBakiye = hareket.SonBakiye.ToMapAmountCariBakiye(),
                    ValorTarihi = hareket.ValorTarihi,
                    IslemTarihi = hareket.IslemTarihi,
                    KarsiHesapIban = hareket.KarsiHesapIBAN,
                    Aciklama = hareket.Aciklama,
                    TutarBorcAlacak = hareket.TutarBorcAlacak.Trim(),
                    FonksiyonKodu = hareket.FonksiyonKodu,
                    Mt940FonksiyonKo = hareket.MT940FonksiyonKodu,
                    FisNo = hareket.FisNo,
                    AmirVkn = hareket.AmirVKN,
                    AmirTckn = hareket.AmirTCKN,
                    HareketKey = hareket.TimeStamp,
                    MuhasebeTarih = "",
                    SiraNo = hareket.ReferansNo,
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.ReferansNo,
                    GondAdi = "",
                    BorcluIban = hareket.BorcluIBAN,
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = "",
                    HesapCinsi = "",
                    SonBakiye = hareket.SonBakiye.ToMapAmount(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = hareket.SonBakiyeBorcAlacak.Trim(),
                    FaizOrani = "",
                    AlacakliIban = hareket.AlacakliIBAN,
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.TimeStamp,
                    Vkn = hareket.VKN
                });
            }
            return list;
        }
    }
}
