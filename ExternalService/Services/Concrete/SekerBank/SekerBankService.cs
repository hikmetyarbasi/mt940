﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;
using ExternalService.Model;
using ExternalService.Model.IsBank;
using ExternalService.Model.SekerBank;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalService.Services.Concrete.IsBankasi.Utils;
using ExternalService.Services.Concrete.SekerBank.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Newtonsoft.Json;
using tr.com.seker.api;

namespace ExternalService.Services.Concrete.SekerBank
{
    public class SekerBankService : BankService
    {
        private tr.com.seker.api.SekerbankNakitYonetimiServisleriSoapClient _client;
        private readonly string username = "20100153";
        private readonly string password = "32G2W4NQ6T";
        private readonly string customerNo = "20100153";
        public SekerBankService()
        {
            var url = "https://nakityonetimi.sekerbank.com.tr/SekerbankNakitYonetimiWebservisleri/CashManagement.asmx";
            //this.isServiceUp = CheckServiceAvailability(url);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.OpenTimeout=TimeSpan.MaxValue;
            binding.CloseTimeout=TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new SekerbankNakitYonetimiServisleriSoapClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\cer\\sekerbank.cer");
            _client.Endpoint.EndpointBehaviors.Add(logbehavior);
            
            _client.ClientCredentials.UserName.UserName = "20100153";
            _client.ClientCredentials.UserName.Password = "32G2W4NQ6T";
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            var resp = new tr.com.seker.api.GetTransactionsByTransactionDateResponse();
            try
            {
                resp = _client.GetTransactionsByTransactionDateAsync
                (
                username,
                password,
                customerNo,
                request.RequestedObject.AccountNumber,
                request.RequestedObject.StartDate.ToFormatDate(),
                request.RequestedObject.EndDate.ToFormatDate()
                ).Result;
                XmlSerializer serializer2 = new XmlSerializer(typeof(SekerBankResponse));
                SekerBankResponse info;
                var XmlFirstRow = "<Accounts>" + resp.Body.GetTransactionsByTransactionDateResult.InnerXml + "</Accounts>";
                //string fileName = @"C:\Project\MT940\MT940\docs\sekerbank\sekerbankresponse.txt";
                //XmlFirstRow = File.ReadAllText(fileName);
                TextReader reader2 = new StringReader(XmlFirstRow);
                info = (SekerBankResponse)serializer2.Deserialize(reader2);

                response = new ApiResponse<ResponseObject>
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = info.Accounts.ToMapAccount()
                    }
                    ,
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };
            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = e.Message + "\r InnerException : " + JsonConvert.SerializeObject(e);
            }

            return response;
        }
    }
}
