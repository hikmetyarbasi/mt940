﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalService.Model;
using ExternalService.Model.SekerBank;
using ExternalService.Services.Abstract;
using ExternalServices.Model;
using Hareket = ExternalService.Model.Hareket;

namespace ExternalService.Services.Concrete.SekerBank.Utils
{
    public static class SekerBankExtMapMethods
    {
        public static string ToFormatDate(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return year + "." + month + "." + day;
        }

        public static List<Hesap> ToMapAccount(this List<Account> accounts)
        {
            var list = new List<Hesap>();
            foreach (var account in accounts)
            {
                var item = new Hesap()
                {
                    Hareketler = account.HesapHareketleri?.ToMapHareket(),
                    Iban = account.IBAN,
                    Bakiye = account.Bakiye?.ToMapAmount(),
                    HesapAcilisTarih = account.HesapAcilisTarihi?.ToFormatDateForResponse(),
                    SonHareketTarihi = account.SonHareketTarihi?.ToFormatDateForResponse(),
                    IslemiYapanSube = account.SubeNo,
                    Blokemeblag = account.KrediLimiti?.ToMapAmount(),
                    KullanilabilirBa = account.Bakiye,
                    MusteriNo = account.MusteriNo,
                    AcilisIlkBakiye = "",
                    DovizTipi = account.DovizKodu,
                    HesapNo = account.HesapNo,
                    KapanisBakiyesi = "",
                    SubeAdi = account.SubeAdi,
                    SubeKodu = account.SubeNo,
                };

                list.Add(item);
            };
            return list;
        }

        public static List<Hareket> ToMapHareket(this List<Model.SekerBank.Hareket> harekets)
        {
            var list = new List<Hareket>();


            foreach (var hareket in harekets)
            {
                var item = new Hareket()
                {
                    Tutar = hareket.HareketTutari.Trim().ToMapAmount(),
                    Shkzg = hareket.BorcAlacak.Trim() == "D" ? "S" : "H",
                    Satir86 = hareket.Aciklama,
                    CariBakiye = hareket.KapanisBakiye,//getCariBakiye(hareket.AcilisBakiye, hareket.HareketTutari, hareket.BorcAlacak).ToMapAmount(),
                    ValorTarihi = "",
                    IslemTarihi = hareket.HareketTarihi.ToFormatDateForResponse(),
                    KarsiHesapIban = "",
                    Aciklama = hareket.Aciklama,
                    TutarBorcAlacak = hareket.BorcAlacak.Trim() == "D" ? "-" : "+",
                    FonksiyonKodu = hareket.IslemKodu,
                    Mt940FonksiyonKo = hareket.IslemKodu,
                    FisNo = hareket.ReferansNo,
                    AmirVkn = hareket.KarsiHesapVKNTCKN,
                    AmirTckn = hareket.KarsiHesapVKNTCKN,
                    HareketKey = hareket.IslemID,
                    MuhasebeTarih = hareket.HareketTarihi.ToFormatDateForResponse(),
                    SiraNo = hareket.SiraNo,
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.ReferansNo,
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = hareket.IslemID,
                    IslemKodu = hareket.IslemKodu,
                    HesapCinsi = "",
                    SonBakiye = hareket.KapanisBakiye,
                    IslemKanali = hareket.KanalKodu,
                    IslemOncesiBakiye = hareket.AcilisBakiye,
                    HesapAdi = "",
                    AcilisGunBakiye = hareket.AcilisBakiye,
                    KapanisBakiye = hareket.KapanisBakiye,
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.HareketTarihi.ToFormatDateWithHhForResponse(hareket.HareketSaati)+hareket.SiraNo,
                    Vkn = hareket.KarsiHesapVKNTCKN
                };
                list.Add(item);
            }

            return list;
        }

        private static string getCariBakiye(string acilisBakiye, string hareketTutari, string borcalacak)
        {
            switch (borcalacak)
            {
                case "D":
                    return (Convert.ToDecimal(acilisBakiye) - Convert.ToDecimal(hareketTutari)).ToString();
                case "C":
                    return (Convert.ToDecimal(acilisBakiye) + Convert.ToDecimal(hareketTutari)).ToString();
                default:
                    return "";
            }
        }

        public static string ToManipulateShkzg(this string borcAlacak)
        {
            if (String.IsNullOrEmpty(borcAlacak)) return "";
            switch (borcAlacak.Substring(0, 1))
            {
                case "-":
                    return "S";
                default:
                    return "H";
            }

        }
    }
}
