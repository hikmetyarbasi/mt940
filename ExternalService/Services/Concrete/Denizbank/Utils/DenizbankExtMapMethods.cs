﻿using System;
using System.Collections.Generic;
using System.Text;
using ExternalService.Model;
using ExternalService.Services.Abstract;
using tr.com.denizbank.api;

namespace ExternalService.Services.Concrete.Denizbank.Utils
{
    public static class DenizbankExtMapMethods
    {
        public static string ToFormatDateForResponse(this string date)
        {
            //22/06/2004
            var year = date.Substring(6, 4);
            var month = date.Substring(3, 2);
            var day = date.Substring(0, 2);
            return year + month + day;
        }
        
        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", ".").Replace("-", "").Replace("+", "");
            return amount;
        }

        public static string ToManipulateShkzg(this string amount)
        {

            if (amount.StartsWith("+"))
            {
                return "H";
            }
            else if (amount.StartsWith("-"))
            {
                return "S";
            }
            else
            {
                return "H";
            }
        }

        public static DateTime toFormatDate(this string date, string time)
        {
            return new DateTime(
                Convert.ToInt32(date.Substring(0, 4)),
                Convert.ToInt32(date.Substring(4, 2)),
                Convert.ToInt32(date.Substring(6, 2)),
                Convert.ToInt32(time.Substring(0, 2)),
                Convert.ToInt32(time.Substring(2, 2)),
                Convert.ToInt32(time.Substring(4, 2))
            );
        }


        public static List<Hesap> toMapHesap(this List<HesapBilgisiDetail> hesaplar,DateTime startdate)
        {
            var list = new List<Hesap>();
            foreach (var hesap in hesaplar)
            {
                var item = new Hesap()
                {
                    Hareketler = hesap.HesapHareketleri.ToMapHareket(startdate),
                    Iban = "",
                    Bakiye = hesap.HesapTanimi.Bakiye.ToString().ToMapAmount(),
                    HesapAcilisTarih = hesap.HesapTanimi.AcilisTarihi.ToFormatDateForResponse(),
                    SonHareketTarihi = hesap.HesapTanimi.SonHareketTarihi.ToFormatDateForResponse(),
                    IslemiYapanSube = hesap.HesapTanimi.SubeNumarasi.ToString(),
                    Blokemeblag = "",
                    KullanilabilirBa = hesap.HesapTanimi.Bakiye.ToString().ToMapAmount(),
                    MusteriNo = hesap.HesapTanimi.MusteriNo,
                    AcilisIlkBakiye = hesap.HesapTanimi.Bakiye.ToString().ToMapAmount(),
                    DovizTipi = "",
                    HesapNo = hesap.HesapTanimi.HesapNumarasi.ToString(),
                    KapanisBakiyesi = hesap.HesapTanimi.Bakiye.ToString().ToMapAmount(),
                    SubeAdi = "",
                    SubeKodu = ""
                };
                list.Add(item);
            }

            return list;
        }
        public static List<Hareket> ToMapHareket(this HesapHareketiDetail[] detaylar,DateTime startdate)
        {
            var list = new List<Hareket>();
            foreach (var hareket in detaylar)
            {

                if (hareket.Tarih != startdate.ToString("dd/MM/yyyy")) continue;
                list.Add(new Hareket()
                {
                    Tutar = hareket.HareketTutari.Trim().ToMapAmount(),
                    Shkzg = hareket.HareketTutari.Trim().ToManipulateShkzg(),
                    Satir86 = hareket.Aciklamalar,
                    CariBakiye = Decimal.Add(hareket.SonBakiye , Convert.ToDecimal(hareket.HareketTutari)).ToString().ToMapAmount(),
                    ValorTarihi = hareket.Tarih.ToFormatDateForResponse(),
                    IslemTarihi = hareket.Tarih.ToFormatDateForResponse(),
                    KarsiHesapIban = "",
                    Aciklama = hareket.Aciklamalar,
                    TutarBorcAlacak = hareket.HareketTutari.Trim().StartsWith("-") ? "-":"+",
                    FonksiyonKodu = hareket.IslemKodu,
                    Mt940FonksiyonKo = hareket.IslemKodu,
                    FisNo = hareket.DekontNo,
                    AmirVkn = "",
                    AmirTckn = "",
                    HareketKey = "",
                    MuhasebeTarih = "",
                    SiraNo = hareket.SiraNo.ToString(),
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.ReferansNo,
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.IslemKodu,
                    HesapCinsi = "",
                    SonBakiye = hareket.SonBakiye.ToString().ToMapAmount(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = hareket.EkBilgi2,
                    LastTmSt =hareket.Tarih+hareket.Saat+hareket.SiraNo,
                    Vkn = ""
                });
            }
            return list;
        }
    }
}
