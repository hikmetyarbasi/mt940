﻿using ExternalService.Model;
using ExternalService.Model.Denizbank;
using ExternalService.Model.IsBank;
using ExternalService.Services.Concrete.Denizbank.Utils;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;
using tr.com.denizbank.api;

namespace ExternalService.Services.Concrete.Denizbank
{
    class DenizBankService : BankService
    {

        private AccountServiceSoapClient _client = null;
        public IsBankaUser _user
        {
            get;
            set;
        }
        private IsBankaUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new IsBankaUser()
            {
                UserDetail = new ExternalService.Model.IsBank.Detail()
                {
                    UserName = enumerable?.SingleOrDefault(p => p.Type == "UserName")?.Value,
                    Password = enumerable?.SingleOrDefault(p => p.Type == "Password")?.Value
                }
            };
        }
        public DenizBankService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            var url = "http://195.85.255.92:160/AccountService.asmx";
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.None;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            var logbehavior = new LogBehavior();
            _client = new AccountServiceSoapClient(binding, endpoint);
            //_client.Endpoint.EndpointBehaviors.Add(logbehavior);

            _client.ClientCredentials.UserName.UserName = "AYGAZ_459454_EXT";
            _client.ClientCredentials.UserName.Password = "4e63696a - 1";
        }
        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {

            var response = new ApiResponse<ResponseObject>();

            try
            {
                var resp = _client.AccountReportAsync(_user.UserDetail.UserName, _user.UserDetail.UserName, _user.UserDetail.Password, request.RequestedObject.StartDate.toFormatDate(request.RequestedObject.StartTime), request.RequestedObject.EndDate.toFormatDate(request.RequestedObject.EndTime)).Result;

                //XmlSerializer serializer2 = new XmlSerializer(typeof(DenizbankResponse));
                //string fileName = @"C:\Project\MT940\MT940\docs\Denizbank\denizbank response.txt";
                //string XmlFirstRow = File.ReadAllText(fileName);
                //TextReader reader = new StringReader(XmlFirstRow);
                //DenizbankResponse resp = (DenizbankResponse)serializer2.Deserialize(reader);

                List<HesapBilgisiDetail> filteredData = (from item in resp.AccountDetail?[0].BankaHesaplari
                                                         where item.HesapTanimi.HesapNumarasi.Replace("-", "").Contains(request.RequestedObject.AccountNumber.TrimStart('0'))
                                                         select item).ToList();

                response = new ApiResponse<ResponseObject>()
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = filteredData?.ToList().toMapHesap(request.RequestedObject.StartDate.toFormatDate(request.RequestedObject.StartTime))
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };


            }
            catch (Exception e)
            {

                response.StatusCode = "1";
                response.StatusMessage = JsonConvert.SerializeObject(e);
            }

            return response;
        }
    }
}
