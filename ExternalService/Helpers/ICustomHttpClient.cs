﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExternalService.Helpers
{
    public interface ICustomHttpClient
    {
        Task<T2> PostRequest<T1, T2>(string v, T1 request);
    }
}
