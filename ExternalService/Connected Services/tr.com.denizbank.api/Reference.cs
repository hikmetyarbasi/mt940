﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace tr.com.denizbank.api
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="tr.com.denizbank.api.AccountServiceSoap")]
    public interface AccountServiceSoap
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/AccountReport", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<tr.com.denizbank.api.Accounts> AccountReportAsync(string associationCode, string user, string password, System.DateTime startDate, System.DateTime endDate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SorgulaOzetWS", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<System.Xml.XmlNode> SorgulaOzetWSAsync(string user, string password, string iban, string startDate, string endDate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SorgulaDetayWS", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<System.Xml.XmlNode> SorgulaDetayWSAsync(string user, string password, string iban, string startDate, string endDate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetSwiftMessage", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<string> GetSwiftMessageAsync(string user, string password, long messageId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetReceiptPictureStream", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<tr.com.denizbank.api.GetReceiptPictureStreamResponse> GetReceiptPictureStreamAsync(tr.com.denizbank.api.GetReceiptPictureStreamRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Accounts
    {
        
        private string errorMessageField;
        
        private BankaHesaplariClassBasic[] accountBasicField;
        
        private BankaHesaplariClassDetail[] accountDetailField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=1)]
        public BankaHesaplariClassBasic[] AccountBasic
        {
            get
            {
                return this.accountBasicField;
            }
            set
            {
                this.accountBasicField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=2)]
        public BankaHesaplariClassDetail[] AccountDetail
        {
            get
            {
                return this.accountDetailField;
            }
            set
            {
                this.accountDetailField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class BankaHesaplariClassBasic
    {
        
        private string sysTarihField;
        
        private string sysSaatField;
        
        private string hataKoduField;
        
        private string hataAciklamaField;
        
        private HesapBilgisiBasic[] bankaHesaplariField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string SysTarih
        {
            get
            {
                return this.sysTarihField;
            }
            set
            {
                this.sysTarihField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string SysSaat
        {
            get
            {
                return this.sysSaatField;
            }
            set
            {
                this.sysSaatField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string HataKodu
        {
            get
            {
                return this.hataKoduField;
            }
            set
            {
                this.hataKoduField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string HataAciklama
        {
            get
            {
                return this.hataAciklamaField;
            }
            set
            {
                this.hataAciklamaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=4)]
        public HesapBilgisiBasic[] BankaHesaplari
        {
            get
            {
                return this.bankaHesaplariField;
            }
            set
            {
                this.bankaHesaplariField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HesapBilgisiBasic
    {
        
        private HesapTanimiClassBasic hesapTanimiField;
        
        private HesapHareketiBasic[] hesapHareketleriField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public HesapTanimiClassBasic HesapTanimi
        {
            get
            {
                return this.hesapTanimiField;
            }
            set
            {
                this.hesapTanimiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=1)]
        public HesapHareketiBasic[] HesapHareketleri
        {
            get
            {
                return this.hesapHareketleriField;
            }
            set
            {
                this.hesapHareketleriField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HesapTanimiClassBasic
    {
        
        private string hesapCinsiField;
        
        private string hesapNumarasiField;
        
        private int subeNumarasiField;
        
        private string subeAdiField;
        
        private string acilisTarihiField;
        
        private string sonHareketTarihiField;
        
        private decimal bakiyeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string HesapCinsi
        {
            get
            {
                return this.hesapCinsiField;
            }
            set
            {
                this.hesapCinsiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string HesapNumarasi
        {
            get
            {
                return this.hesapNumarasiField;
            }
            set
            {
                this.hesapNumarasiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int SubeNumarasi
        {
            get
            {
                return this.subeNumarasiField;
            }
            set
            {
                this.subeNumarasiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string SubeAdi
        {
            get
            {
                return this.subeAdiField;
            }
            set
            {
                this.subeAdiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string AcilisTarihi
        {
            get
            {
                return this.acilisTarihiField;
            }
            set
            {
                this.acilisTarihiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string SonHareketTarihi
        {
            get
            {
                return this.sonHareketTarihiField;
            }
            set
            {
                this.sonHareketTarihiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public decimal Bakiye
        {
            get
            {
                return this.bakiyeField;
            }
            set
            {
                this.bakiyeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HesapHareketiDetail
    {
        
        private string tarihField;
        
        private string saatField;
        
        private int siraNoField;
        
        private string hareketTutariField;
        
        private decimal sonBakiyeField;
        
        private string aciklamalarField;
        
        private string musteriNoField;
        
        private string islemKoduField;
        
        private string referansNoField;
        
        private string karsiHesapVKNoField;
        
        private string dekontNoField;
        
        private string ekBilgi1Field;
        
        private string ekBilgi2Field;
        
        private string ekBilgi3Field;
        
        private string ekBilgi4Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Tarih
        {
            get
            {
                return this.tarihField;
            }
            set
            {
                this.tarihField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Saat
        {
            get
            {
                return this.saatField;
            }
            set
            {
                this.saatField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int SiraNo
        {
            get
            {
                return this.siraNoField;
            }
            set
            {
                this.siraNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string HareketTutari
        {
            get
            {
                return this.hareketTutariField;
            }
            set
            {
                this.hareketTutariField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public decimal SonBakiye
        {
            get
            {
                return this.sonBakiyeField;
            }
            set
            {
                this.sonBakiyeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string Aciklamalar
        {
            get
            {
                return this.aciklamalarField;
            }
            set
            {
                this.aciklamalarField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string MusteriNo
        {
            get
            {
                return this.musteriNoField;
            }
            set
            {
                this.musteriNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string IslemKodu
        {
            get
            {
                return this.islemKoduField;
            }
            set
            {
                this.islemKoduField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string ReferansNo
        {
            get
            {
                return this.referansNoField;
            }
            set
            {
                this.referansNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string KarsiHesapVKNo
        {
            get
            {
                return this.karsiHesapVKNoField;
            }
            set
            {
                this.karsiHesapVKNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public string DekontNo
        {
            get
            {
                return this.dekontNoField;
            }
            set
            {
                this.dekontNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=11)]
        public string EkBilgi1
        {
            get
            {
                return this.ekBilgi1Field;
            }
            set
            {
                this.ekBilgi1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=12)]
        public string EkBilgi2
        {
            get
            {
                return this.ekBilgi2Field;
            }
            set
            {
                this.ekBilgi2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=13)]
        public string EkBilgi3
        {
            get
            {
                return this.ekBilgi3Field;
            }
            set
            {
                this.ekBilgi3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=14)]
        public string EkBilgi4
        {
            get
            {
                return this.ekBilgi4Field;
            }
            set
            {
                this.ekBilgi4Field = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HesapTanimiClassDetail
    {
        
        private string hesapTuruField;
        
        private string hesapAdiField;
        
        private string musteriNoField;
        
        private string hesapCinsiField;
        
        private string hesapNumarasiField;
        
        private int subeNumarasiField;
        
        private string subeAdiField;
        
        private string acilisTarihiField;
        
        private string sonHareketTarihiField;
        
        private decimal bakiyeField;
        
        private decimal blokeMeblagField;
        
        private decimal kullanilabilirBakiyeField;
        
        private decimal krediLimitiField;
        
        private decimal krediliKullanilabilirBakiyeField;
        
        private string vadeTarihiField;
        
        private decimal faizOraniField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string HesapTuru
        {
            get
            {
                return this.hesapTuruField;
            }
            set
            {
                this.hesapTuruField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string HesapAdi
        {
            get
            {
                return this.hesapAdiField;
            }
            set
            {
                this.hesapAdiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string MusteriNo
        {
            get
            {
                return this.musteriNoField;
            }
            set
            {
                this.musteriNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string HesapCinsi
        {
            get
            {
                return this.hesapCinsiField;
            }
            set
            {
                this.hesapCinsiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string HesapNumarasi
        {
            get
            {
                return this.hesapNumarasiField;
            }
            set
            {
                this.hesapNumarasiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public int SubeNumarasi
        {
            get
            {
                return this.subeNumarasiField;
            }
            set
            {
                this.subeNumarasiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string SubeAdi
        {
            get
            {
                return this.subeAdiField;
            }
            set
            {
                this.subeAdiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string AcilisTarihi
        {
            get
            {
                return this.acilisTarihiField;
            }
            set
            {
                this.acilisTarihiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string SonHareketTarihi
        {
            get
            {
                return this.sonHareketTarihiField;
            }
            set
            {
                this.sonHareketTarihiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public decimal Bakiye
        {
            get
            {
                return this.bakiyeField;
            }
            set
            {
                this.bakiyeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public decimal BlokeMeblag
        {
            get
            {
                return this.blokeMeblagField;
            }
            set
            {
                this.blokeMeblagField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=11)]
        public decimal KullanilabilirBakiye
        {
            get
            {
                return this.kullanilabilirBakiyeField;
            }
            set
            {
                this.kullanilabilirBakiyeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=12)]
        public decimal KrediLimiti
        {
            get
            {
                return this.krediLimitiField;
            }
            set
            {
                this.krediLimitiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=13)]
        public decimal KrediliKullanilabilirBakiye
        {
            get
            {
                return this.krediliKullanilabilirBakiyeField;
            }
            set
            {
                this.krediliKullanilabilirBakiyeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=14)]
        public string VadeTarihi
        {
            get
            {
                return this.vadeTarihiField;
            }
            set
            {
                this.vadeTarihiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=15)]
        public decimal FaizOrani
        {
            get
            {
                return this.faizOraniField;
            }
            set
            {
                this.faizOraniField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HesapBilgisiDetail
    {
        
        private HesapTanimiClassDetail hesapTanimiField;
        
        private HesapHareketiDetail[] hesapHareketleriField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public HesapTanimiClassDetail HesapTanimi
        {
            get
            {
                return this.hesapTanimiField;
            }
            set
            {
                this.hesapTanimiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=1)]
        public HesapHareketiDetail[] HesapHareketleri
        {
            get
            {
                return this.hesapHareketleriField;
            }
            set
            {
                this.hesapHareketleriField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class BankaHesaplariClassDetail
    {
        
        private string sysTarihField;
        
        private string sysSaatField;
        
        private string hataKoduField;
        
        private string hataAciklamaField;
        
        private HesapBilgisiDetail[] bankaHesaplariField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string SysTarih
        {
            get
            {
                return this.sysTarihField;
            }
            set
            {
                this.sysTarihField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string SysSaat
        {
            get
            {
                return this.sysSaatField;
            }
            set
            {
                this.sysSaatField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string HataKodu
        {
            get
            {
                return this.hataKoduField;
            }
            set
            {
                this.hataKoduField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string HataAciklama
        {
            get
            {
                return this.hataAciklamaField;
            }
            set
            {
                this.hataAciklamaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=4)]
        public HesapBilgisiDetail[] BankaHesaplari
        {
            get
            {
                return this.bankaHesaplariField;
            }
            set
            {
                this.bankaHesaplariField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HesapHareketiBasic
    {
        
        private string tarihField;
        
        private int siraNoField;
        
        private string hareketTutariField;
        
        private decimal sonBakiyeField;
        
        private string aciklamalarField;
        
        private string musteriNumarasiField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Tarih
        {
            get
            {
                return this.tarihField;
            }
            set
            {
                this.tarihField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int SiraNo
        {
            get
            {
                return this.siraNoField;
            }
            set
            {
                this.siraNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string HareketTutari
        {
            get
            {
                return this.hareketTutariField;
            }
            set
            {
                this.hareketTutariField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public decimal SonBakiye
        {
            get
            {
                return this.sonBakiyeField;
            }
            set
            {
                this.sonBakiyeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string Aciklamalar
        {
            get
            {
                return this.aciklamalarField;
            }
            set
            {
                this.aciklamalarField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string MusteriNumarasi
        {
            get
            {
                return this.musteriNumarasiField;
            }
            set
            {
                this.musteriNumarasiField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetReceiptPictureStream", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetReceiptPictureStreamRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string user;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public string password;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=2)]
        public string refNum;
        
        public GetReceiptPictureStreamRequest()
        {
        }
        
        public GetReceiptPictureStreamRequest(string user, string password, string refNum)
        {
            this.user = user;
            this.password = password;
            this.refNum = refNum;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetReceiptPictureStreamResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetReceiptPictureStreamResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] GetReceiptPictureStreamResult;
        
        public GetReceiptPictureStreamResponse()
        {
        }
        
        public GetReceiptPictureStreamResponse(byte[] GetReceiptPictureStreamResult)
        {
            this.GetReceiptPictureStreamResult = GetReceiptPictureStreamResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface AccountServiceSoapChannel : tr.com.denizbank.api.AccountServiceSoap, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class AccountServiceSoapClient : System.ServiceModel.ClientBase<tr.com.denizbank.api.AccountServiceSoap>, tr.com.denizbank.api.AccountServiceSoap
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public AccountServiceSoapClient(EndpointConfiguration endpointConfiguration) : 
                base(AccountServiceSoapClient.GetBindingForEndpoint(endpointConfiguration), AccountServiceSoapClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public AccountServiceSoapClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(AccountServiceSoapClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public AccountServiceSoapClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(AccountServiceSoapClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public AccountServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<tr.com.denizbank.api.Accounts> AccountReportAsync(string associationCode, string user, string password, System.DateTime startDate, System.DateTime endDate)
        {
            return base.Channel.AccountReportAsync(associationCode, user, password, startDate, endDate);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> SorgulaOzetWSAsync(string user, string password, string iban, string startDate, string endDate)
        {
            return base.Channel.SorgulaOzetWSAsync(user, password, iban, startDate, endDate);
        }
        
        public System.Threading.Tasks.Task<System.Xml.XmlNode> SorgulaDetayWSAsync(string user, string password, string iban, string startDate, string endDate)
        {
            return base.Channel.SorgulaDetayWSAsync(user, password, iban, startDate, endDate);
        }
        
        public System.Threading.Tasks.Task<string> GetSwiftMessageAsync(string user, string password, long messageId)
        {
            return base.Channel.GetSwiftMessageAsync(user, password, messageId);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<tr.com.denizbank.api.GetReceiptPictureStreamResponse> tr.com.denizbank.api.AccountServiceSoap.GetReceiptPictureStreamAsync(tr.com.denizbank.api.GetReceiptPictureStreamRequest request)
        {
            return base.Channel.GetReceiptPictureStreamAsync(request);
        }
        
        public System.Threading.Tasks.Task<tr.com.denizbank.api.GetReceiptPictureStreamResponse> GetReceiptPictureStreamAsync(string user, string password, string refNum)
        {
            tr.com.denizbank.api.GetReceiptPictureStreamRequest inValue = new tr.com.denizbank.api.GetReceiptPictureStreamRequest();
            inValue.user = user;
            inValue.password = password;
            inValue.refNum = refNum;
            return ((tr.com.denizbank.api.AccountServiceSoap)(this)).GetReceiptPictureStreamAsync(inValue);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.AccountServiceSoap))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            if ((endpointConfiguration == EndpointConfiguration.AccountServiceSoap12))
            {
                System.ServiceModel.Channels.CustomBinding result = new System.ServiceModel.Channels.CustomBinding();
                System.ServiceModel.Channels.TextMessageEncodingBindingElement textBindingElement = new System.ServiceModel.Channels.TextMessageEncodingBindingElement();
                textBindingElement.MessageVersion = System.ServiceModel.Channels.MessageVersion.CreateVersion(System.ServiceModel.EnvelopeVersion.Soap12, System.ServiceModel.Channels.AddressingVersion.None);
                result.Elements.Add(textBindingElement);
                System.ServiceModel.Channels.HttpTransportBindingElement httpBindingElement = new System.ServiceModel.Channels.HttpTransportBindingElement();
                httpBindingElement.AllowCookies = true;
                httpBindingElement.MaxBufferSize = int.MaxValue;
                httpBindingElement.MaxReceivedMessageSize = int.MaxValue;
                result.Elements.Add(httpBindingElement);
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.AccountServiceSoap))
            {
                return new System.ServiceModel.EndpointAddress("http://195.85.255.92:160/AccountService.asmx");
            }
            if ((endpointConfiguration == EndpointConfiguration.AccountServiceSoap12))
            {
                return new System.ServiceModel.EndpointAddress("http://195.85.255.92:160/AccountService.asmx");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        public enum EndpointConfiguration
        {
            
            AccountServiceSoap,
            
            AccountServiceSoap12,
        }
    }
}
