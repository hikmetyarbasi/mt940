﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using ExternalServices.Services.Abstract;

namespace ExternalServices.Factory.Abstract
{
    public abstract class BankFactory
    {
        public abstract BankService GetService(IEnumerable<Claim> user);
    }
}
