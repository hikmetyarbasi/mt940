﻿using System.Collections.Generic;
using System.Security.Claims;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using ExternalServices.Services.Concrete;
using Microsoft.Extensions.Configuration;

namespace ExternalServices.Factory.Concrete
{
    public class YapikrediFactory:BankFactory
    {
        IConfiguration _configuration;
        public YapikrediFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new YapikrediService(_configuration, user);
        }
    }
}
