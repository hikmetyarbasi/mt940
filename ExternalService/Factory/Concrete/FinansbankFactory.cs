﻿using ExternalService.Services.Concrete.Finansbank;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class FinansbankFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            var firmCode = enumerable?.SingleOrDefault(p => p.Type == "FirmCode")?.Value;
            switch (firmCode)
            {
                case "01":
                    return new FinansbankAygazService();
                case "02":
                    return new FinansbankAygazDgService();
                case "03":
                    return new FinansbankAkpaService();
                default: return new FinansbankAygazService();
            }
        }
    }
}
