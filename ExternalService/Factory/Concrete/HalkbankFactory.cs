﻿using ExternalService.Services.Concrete.HalkBankasi;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class HalkbankFactory : BankFactory
    {
        IConfiguration _configuration;
        public HalkbankFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new HalkBankService(_configuration);
        }
    }
}
