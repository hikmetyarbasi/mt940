﻿using ExternalService.Services.Concrete.VakifBank;
using ExternalService.Services.Concrete.Ziraat;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class VakifBankFactory : BankFactory
    {
        IConfiguration _configuration;
        public VakifBankFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new VakifBankService(user);
        }
    }
}
