﻿using ExternalService.Services.Concrete.Hsbc;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class HsbcFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new HsbcService(user);
        }
    }
}
