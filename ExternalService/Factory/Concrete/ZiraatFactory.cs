﻿using ExternalService.Services.Concrete.Ziraat;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class ZiraatFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new ZiraatService(user);
        }
    }
}
