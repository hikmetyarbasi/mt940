﻿using ExternalService.Services.Concrete.Denizbank;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class DenizbankFactory: BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new DenizBankService(user);
        }
    }
}
