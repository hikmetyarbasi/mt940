﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using ExternalService.Services.Concrete.Teb;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;

namespace ExternalService.Factory.Concrete
{
    public class TebFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new TebService();
        }
    }
}
