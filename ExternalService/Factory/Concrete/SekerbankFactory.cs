﻿using ExternalService.Services.Concrete.SekerBank;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace ExternalService.Factory.Concrete
{
    public class SekerbankFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new SekerBankService();
        }
    }
}
