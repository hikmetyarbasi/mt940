﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using ExternalService.Services.Concrete.Akbank;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;

namespace ExternalService.Factory.Concrete
{
    public class AkbankFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new AkbankService(user);
        }
    }
}
