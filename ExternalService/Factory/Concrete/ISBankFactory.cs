﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using ExternalService.Services.Concrete.IsBankasi;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;

namespace ExternalService.Factory.Concrete
{
    public class ISBankFactory : BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new IsBankasiService(user);
        }
    }
}
