﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalService.Services.Abstract
{
    public static class ExtMapMethod
    {
        public static string ToFormatDateForResponse(this string date)
        {
            var year = date.Substring(6, 4);
            var month = date.Substring(3, 2);
            var day = date.Substring(0, 2);
            return year + month + day;
        }
        public static string ToFormatDate(this string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return year + "-" + month + "-" + day;
        }



        public static string ToFormatTime(this string time)
        {
            var hour = time.Substring(0, 2);
            var minute = time.Substring(2, 2);
            var seconds = time.Substring(4, 2);
            return hour + "." + minute + "." + seconds + ".000001";
        }
    }
}
