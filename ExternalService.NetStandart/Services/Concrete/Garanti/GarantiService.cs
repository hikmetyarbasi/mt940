﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using ExternalService.Model;
using ExternalService.Services.Abstract;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;
using ExternalServices.Services.Abstract;
using tr.com.garanti.api;

namespace ExternalService.Services.Concrete.Garanti
{
    public class GarantiService : BankService
    {
        private tr.com.garanti.api.FirmAccountActivitySoapClient _client;
        private GbUser _user;
        public GarantiService(IEnumerable<Claim> user)
        {
            _user = LoadUserInfo(user);
            var url = "https://inboundrstintws.garanti.com.tr/services/FirmAccountActivitySoap";
            //this.isServiceUp = CheckServiceAvailability(url);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            EndpointAddress endpoint = new EndpointAddress(url);
            _client = new FirmAccountActivitySoapClient(binding, endpoint);
            _client.ClientCredentials.ClientCertificate.Certificate = GetCertificate("\\garanti.cer");
        }
        

        public override ApiResponse<ResponseObject> Send(ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            try
            {
                var resp = _client.FirmAccountActivityAsync(new FirmAccountActivityRequest()
                {
                    StartDate = request.RequestedObject.StartDate.ToFormatDate() + "-" + request.RequestedObject.StartTime.ToFormatTime(),
                    BranchNum = request.RequestedObject.Param1,
                    AccountNum = request.RequestedObject.AccountNumber,
                    EndDate = request.RequestedObject.EndDate.ToFormatDate() + "-" + request.RequestedObject.EndTime.ToFormatTime(),
                    FirmCode = _user.Details.FirmCode,
                    IBAN = "",
                    securetoken = new SecureToken()
                    {
                        UserId = _user.Details.Username,
                        CreateTimestamp = "",
                        Encoded = _user.Details.Token,
                        Password = ""
                    },
                    TransactionId = ""
                }).Result;
                response = new ApiResponse<ResponseObject>
                {
                    ResponseObject = new ResponseObject()
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = resp.FirmAccounts?.ToMapAccount()
                    }
                    ,
                    StatusCode = Convert.ToInt32(resp.ReturnVal),
                    StatusMessage = resp.MessageText
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return response;
        }
        protected GbUser LoadUserInfo(IEnumerable<Claim> user)
        {
            var enumerable = user as Claim[] ?? user.ToArray();
            return new GbUser()
            {
                Details = new Detail()
                {
                    Token = enumerable?.SingleOrDefault(p => p.Type == "Token")?.Value,
                    FirmCode = enumerable?.SingleOrDefault(p => p.Type == "FirmCode")?.Value,
                    Username = enumerable?.SingleOrDefault(p => p.Type == "Username")?.Value
                }
            };
        }
        public FirmAccountActivityResponse CreateDummyData()
        {

            return new FirmAccountActivityResponse()
            {
                FirmAccounts = new FirmAccountDetail[]
                {
                    new FirmAccountDetail()
                    {
                        AccountName="",
                        AccountType="S",
                        BranchNum="00382",
                        AccountNum="6295972",
                        CurrencyCode="TL",
                        IBAN="TR960006200038200006295972",
                        OpenDate="09.04.2010",
                        LastActivityDate="15.10.2019",
                        Balance="359,294.16",
                        BlockedAmount="0",
                        AvailableBalance="359,294.16",
                        CreditLimit="0",
                        AvailableBalanceWithCredit="359,294.16",
                        AccountActivities = new AccountActivityDetail[]
                        {
                            new AccountActivityDetail()
                            {
                                TransactionId = "WPSO",
                                Amount = "470,443.28",
                                CorrVKN = "01250356459",
                                Explanation = "ÖD.EMR AYTEMİZ AKARYAKIT DAĞITIM ANON ÖDEMESİ",
                                TransactionReferenceId = "2019-10-14-05.38.01.244143",
                                CorrIBAN = "0",
                                Balance = "1,726,997.68",
                                ActivityDate = "14.10.2019",
                                CorrBankNum = "00000",
                                CorrBranchNum = "00000"
                            },
                            new AccountActivityDetail()
                            {
                                ActivityDate = "14.10.2019",
                                Amount="10,369.82",
                                Balance="1,737,367.5",
                                Explanation = "INT-HVL-ÇAPANOĞLU PETROL ORM",
                                TransactionId="AT03",
                                TransactionReferenceId="2019-10-14-08.30.44.760275",
                                CorrBankNum= "00000",
                                CorrBranchNum= "00485",
                                CorrIBAN= "TR750006200048500006299812",
                                CorrVKN = "02300067816"
                            },
                            new AccountActivityDetail()
                            {
                                ActivityDate="14.10.2019",
                                Amount="11,295.99",
                                Balance="1,748,663.49",
                                Explanation="INT-HVL-ÇAPANOĞLU PETROL ORM",
                                TransactionId="AT03",
                                TransactionReferenceId="2019-10-14-08.30.44.871293",
                                CorrBankNum="00000",
                                CorrBranchNum="00485",
                                CorrIBAN="TR750006200048500006299812",
                                CorrVKN="02300067816"
                            },
                            new AccountActivityDetail()
                            {
                                ActivityDate="14.10.2019",
                                Amount="17,297.44",
                                Balance="1,765,960.93",
                                Explanation="INT-HVL-ÇAPANOĞLU PETROL ORM",
                                TransactionId="AT03</n0:TransactionId",
                                TransactionReferenceId="2019-10-14-08.30.44.974309",
                                CorrBankNum="00000",
                                CorrBranchNum="00485",
                                CorrIBAN="TR750006200048500006299812        ",
                                CorrVKN="02300067816",
                            },
                            new AccountActivityDetail()
                            {
                               ActivityDate="14.10.2019",
                               Amount="13,020",
                               Balance="1,778,980.93",
                               Explanation="INT-HVLEMR-LPG ALIM ODEMESİ AVCI PETROL",
                               TransactionId="WPSO",
                               TransactionReferenceId="2019-10-14-08.38.46.284277",
                               CorrBankNum="00000",
                               CorrBranchNum="01182",
                               CorrIBAN="TR330006200118200006299217        ",
                               CorrVKN="01040085869",
                            },
                            new AccountActivityDetail()
                            {
                               ActivityDate="14.10.2019",
                               Amount="30,424",
                               Balance="1,809,404.93",
                               Explanation="INT-HVLEMR-LPG ALIM ODEMESİ AVCI PETROL",
                               TransactionId="WPSO",
                               TransactionReferenceId="2019-10-14-08.38.48.301628",
                               CorrBankNum="00000",
                               CorrBranchNum="01182",
                               CorrIBAN="TR330006200118200006299217        ",
                               CorrVKN="01040085869",
                            },
                        }
                    }
                }
            };
        }
    }
}