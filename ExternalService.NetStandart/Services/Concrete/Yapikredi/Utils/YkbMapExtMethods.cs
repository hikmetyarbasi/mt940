﻿using System.Collections.Generic;
using System.Linq;
using ExternalService.Services.Abstract;
using ExternalService.Services.Concrete.Garanti.Utils;
using ExternalServices.Model;
using tr.com.yapikredi.api;

namespace ExternalService.Services.Concrete.Yapikredi.Utils
{
    public static class YkbMapExtMethods
    {
        public static string ToManipulateShkzg(this string borcAlacak)
        {
            switch (borcAlacak)
            {
                case "A":
                    return "H";
                case "B":
                    return "S";
                default:
                    return "";
            }
        }

        public static string ToTutarBorcAlacak(this string borcAlacak)
        {
            switch (borcAlacak)
            {
                case "A":
                    return "+";
                case "B":
                    return "-";
                default:
                    return "";
            }
        }

        public static List<Hesap> ToMapHesap(this ehoAccountTransactionHesapDTO[] hesap)
        {
            return hesap.Select(item => new Hesap
            {
                AcilisIlkBakiye = item.acilisBakiyesi.ToMapAmount(),
                DovizTipi = item.dovizTipi,
                HesapNo = item.hesapNo,
                KapanisBakiyesi = item.kapanisBakiyesi.ToMapAmount(),
                SubeAdi = item.subeAdi,
                SubeKodu = item.subeKodu,
                Hareketler = item.hareketler.hareket.ToMapHareket(),
                Bakiye = "",
                Blokemeblag = "",
                HesapAcilisTarih = "",
                Iban = "",
                IslemiYapanSube = "",
                KullanilabilirBa = "",
                MusteriNo = "",
                SonHareketTarihi = ""
            }).ToList();
        }

        public static List<Hareket> ToMapHareket(this ehoAccountTransactionHareketDTO[] hareket)
        {
            return hareket.Select(item => new Hareket()
            {
                Tutar = item.tutar,
                Shkzg = item.borcAlacak.ToManipulateShkzg(),
                Satir86 = item.aciklama,
                CariBakiye = item.anlikBakiye.ToMapAmount(),
                ValorTarihi = item.valor,
                IslemTarihi = "20" + item.fizikselIslemTarihi,
                KarsiHesapIban = item.gonderenIbanNo,
                Aciklama = item.karsiHesapVNo,
                TutarBorcAlacak = item.borcAlacak.ToTutarBorcAlacak(),
                FonksiyonKodu = item.islemTipi,
                Mt940FonksiyonKo = item.islemTipi,
                FisNo = item.dekontNo,
                AmirVkn = item.karsiHesapVNo,
                AmirTckn = item.karsiHesapVNo,
                HareketKey = item.hareketKey,
                MuhasebeTarih = "20" + item.muhasebeTarihi,
                SiraNo = item.siraNo,
                KrediliKulBak = "",
                KrediLimit = "",
                ReferenceNo = "",
                GondAdi = "",
                BorcluIban = "",
                GondHesapNo = "",
                Urf = "",
                IslemAdi = "",
                IslemKodu = "",
                HesapCinsi = "",
                SonBakiye = "",
                IslemKanali = "",
                IslemOncesiBakiye = "",
                HesapAdi = "",
                AcilisGunBakiye = "",
                KapanisBakiye = "",
                Hata = "",
                HesapTuru = "",
                SonBakiyeBorcAla = "",
                FaizOrani = "",
                AlacakliIban = "",
                AliciHesapNo = "",
                HavaleRefNo = "",
                VadeTarihi = "",
                DigerMusteriNo = "",
                GondSubeKodu = "",
                HesapTuruKod = "",
                Ekbilgi = "",
                LastTmSt = "",
                Vkn = ""
            }).ToList();
        }
    }
}
