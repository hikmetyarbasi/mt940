﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ExternalServices.Model
{
    public class ResponseObject
    {

        public long Solid { get; set; }
        public string BankaAdi { get; set; }
        public string BankaKodu { get; set; }
        public string BankaVergiDairesi { get; set; }
        public string BankaVergiNumarasi { get; set; }
        public string HataAciklamasi { get; set; }
        public string HataKodu { get; set; }
        public List<Hesap> Hesaplar { get; set; }
    }

    public class Hesap
    {
        public string AcilisIlkBakiye { get; set; }
        public string DovizTipi { get; set; }
        public string KapanisBakiyesi { get; set; }
        public string HesapNo { get; set; }
        public string SubeAdi { get; set; }
        public string SubeKodu { get; set; }
        public string Iban { get; set; }
        public string Bakiye { get; set; }
        public string HesapAcilisTarih { get; set; }
        public string SonHareketTarihi { get; set; }
        public string IslemiYapanSube { get; set; }
        public string Blokemeblag { get; set; }
        public string KullanilabilirBa { get; set; }
        public string MusteriNo { get; set; }

        public List<Hareket> Hareketler { get; set; }
    }

    public class Hareket
    {
        public string Tutar { get; set; }
        public string Shkzg { get; set; }
        public string Satir86 { get; set; }
        public string Urf { get; set; }
        public string AcilisGunBakiye { get; set; }
        public string CariBakiye { get; set; }
        public string HesapTuruKod { get; set; }
        public string LastTmSt { get; set; }
        public string ValorTarihi { get; set; }
        public string IslemTarihi { get; set; }
        public string ReferenceNo { get; set; }
        public string KarsiHesapIban { get; set; }
        public string SonBakiye { get; set; }
        public string Aciklama { get; set; }
        public string Vkn { get; set; }
        public string TutarBorcAlacak { get; set; }
        public string SonBakiyeBorcAla { get; set; }
        public string FonksiyonKodu { get; set; }
        public string Mt940FonksiyonKo { get; set; }
        public string FisNo { get; set; }
        public string AmirVkn { get; set; }
        public string AmirTckn { get; set; }
        public string BorcluIban { get; set; }
        public string AlacakliIban { get; set; }
        public string Hata { get; set; }
        public string HareketKey { get; set; }
        public string MuhasebeTarih { get; set; }
        public string SiraNo { get; set; }
        public string KapanisBakiye { get; set; }
        public string IslemOncesiBakiye { get; set; }
        public string IslemAdi { get; set; }
        public string IslemKanali { get; set; }
        public string IslemKodu { get; set; }
        public string HavaleRefNo { get; set; }
        public string GondHesapNo { get; set; }
        public string AliciHesapNo { get; set; }
        public string GondAdi { get; set; }
        public string GondSubeKodu { get; set; }
        public string DigerMusteriNo { get; set; }
        public string FaizOrani { get; set; }
        public string Ekbilgi { get; set; }
        public string HesapAdi { get; set; }
        public string HesapCinsi { get; set; }
        public string HesapTuru { get; set; }
        public string KrediLimit { get; set; }
        public string KrediliKulBak { get; set; }
        public string VadeTarihi { get; set; }
    }
}
