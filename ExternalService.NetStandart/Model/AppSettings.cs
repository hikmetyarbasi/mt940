﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT940.api.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string VirtualDirectory { get; set; }
    }
}
