﻿namespace ExternalServices.Model
{
    public class ApiResponse<T>
    {
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public T ResponseObject { get; set; }

    }
}
