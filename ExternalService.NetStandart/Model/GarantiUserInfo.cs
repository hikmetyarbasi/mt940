﻿namespace ExternalService.Model
{
    public class GarantiUserInfo
    {
        public GbUser[] Users { get; set; }

    }

    public class GbUser
    {
        public string AppUserName { get; set; }
        public string AppPassword { get; set; }
        public Detail Details { get; set; }
    }
    public class Detail
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string FirmCode { get; set; }
        public string Branchnum { get; set; }
    }
}
