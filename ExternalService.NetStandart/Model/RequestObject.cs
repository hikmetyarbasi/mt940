﻿namespace ExternalServices.Model
{
    public class RequestObject
    {
        public string CurrencyCode { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string AccountNumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
