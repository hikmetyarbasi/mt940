﻿using System.Collections.Generic;
using System.Security.Claims;
using ExternalServices.Factory.Abstract;
using ExternalServices.Services.Abstract;
using ExternalServices.Services.Concrete;

namespace ExternalServices.Factory.Concrete
{
    public class YapikrediFactory:BankFactory
    {
        public override BankService GetService(IEnumerable<Claim> user)
        {
            return new YapikrediService(user);
        }
    }
}
