using ExternalService.Services.Abstract;
using NUnit.Framework;
using System;

namespace MT940Test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void ToFormatDateForResponse()
        {
            string date = "03.08.1994";
            var year = date.Substring(6, 4);
            var month = date.Substring(3, 2);
            var day = date.Substring(0, 2);
        }
        public string ToFormatDate(string date)
        {
            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            var day = date.Substring(6, 2);
            return year + "-" + month + "-" + day;
        }
        [Test]
        public void compareDate()
        {
            string timestampt = "2020-06-15-09.00.18.171900",startdate = "2020-06-15", endate = "2020-06-16";
            if (Convert.ToDateTime(timestampt.ToFormatDate2()) >=
                Convert.ToDateTime(startdate) && 
                Convert.ToDateTime(timestampt.ToFormatDate2()) <= Convert.ToDateTime(endate))
            {
                Assert.Pass();
            }
            else { 
            Assert.Fail();
            }
        }

        public string ToFormatTime(string time)
        {
            var hour = time.Substring(0, 2);
            var minute = time.Substring(2, 2);
            var seconds = time.Substring(4, 2);
            return hour + "." + minute + "." + seconds + ".000001";
        }

    }
}