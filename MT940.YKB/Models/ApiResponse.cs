﻿namespace MT940.YKB.Models
{
    public class ApiResponse<T>
    {
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public T ResponseObject { get; set; }

    }
}
