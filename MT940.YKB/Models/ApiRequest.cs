﻿namespace MT940.YKB.Models
{
    public class ApiRequest<T>
    {
        public T RequestedObject { get; set; }
    }
}
