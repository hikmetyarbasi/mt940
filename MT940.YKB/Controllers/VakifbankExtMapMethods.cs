﻿using System;
using System.Collections.Generic;
using System.IO;
using MT940.YKB.Controllers;
using MT940.YKB.Models;
using MT940.YKB.tr.com.vakifbank.api;

namespace ExternalService.Services.Concrete.VakifBank.Utils
{
    public static class VakibBankExtMapMethods
    {
        private static T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
        public static string ToManipulateShkzg(this string amount)
        {

            if (amount.StartsWith("-"))
            {
                return "S";
            }
            else
            {
                return "H";
            }
        }

        public static string toRequestDate(this string date, string time)
        {
            //2020-10-20 00:00
            return date.Substring(0, 4) + "-" + date.Substring(4, 2) + "-" + date.Substring(6, 2) + " " + time.Substring(0, 2) + ":" + time.Substring(2, 2);
        }
        public static List<Hesap> ToMapAccount(this DtoEkstreHesap[] accounts)
        {
            var list = new List<Hesap>();
            if (accounts is null)
                return list;

            foreach (var account in accounts)
            {
                var item = new Hesap()
                {
                    Hareketler = account.Hareketler.ToMapHareket(),
                    Iban = "",
                    Bakiye = "",
                    HesapAcilisTarih = "",
                    SonHareketTarihi = account.SonHareketTarihi.ToFormatDate3(),
                    IslemiYapanSube = account.SubeAdi,
                    Blokemeblag = "",
                    KullanilabilirBa = account.KullanilabilirBakiye.ToString(),
                    MusteriNo = account.MusteriNo,
                    AcilisIlkBakiye = account.AcilisBakiye.ToString(),
                    DovizTipi = account.DovizTipi,
                    HesapNo = account.HesapNo,
                    KapanisBakiyesi = "",
                    SubeAdi = account.SubeAdi,
                    SubeKodu = account.SubeKodu,
                };
                list.Add(item);
            }

            return list;
        }

        public static List<Hareket> ToMapHareket(this DtoEkstreHareket[] harekets)
        {
            var list = new List<Hareket>();

            foreach (var hareket in harekets)
            {
                string SwiftKodu = "", GonderenIbanKumarasi = "";
                hareket.Detaylar?.TryGetValue("GonderenIbanKumarasi", out GonderenIbanKumarasi);
                hareket.Detaylar?.TryGetValue("SwiftKodu", out SwiftKodu);
                list.Add(new Hareket
                {
                    Tutar = hareket.Tutar.ToString().Trim().TrimStart('-').TrimStart('+'),
                    Shkzg = hareket.BorcAlacak == "A" ? "H" : "S",
                    Satir86 = hareket.Aciklama.Trim(),
                    CariBakiye = hareket.IslemSonrasıBakiye.ToString(),
                    ValorTarihi = hareket.IslemTarihi.ToFormatDate3(),
                    IslemTarihi = hareket.IslemTarihi.ToFormatDate3(),
                    KarsiHesapIban = GonderenIbanKumarasi,
                    Aciklama = hareket.Aciklama,
                    TutarBorcAlacak = hareket.Tutar.ToString().Trim().StartsWith("-") ? "-" : "+",
                    FonksiyonKodu = SwiftKodu,
                    Mt940FonksiyonKo = SwiftKodu,
                    FisNo = hareket.IslemNo,
                    AmirVkn = "",
                    AmirTckn = "",
                    HareketKey = hareket.Id.ToString(),
                    MuhasebeTarih = hareket.IslemTarihi.ToFormatDate3(),
                    SiraNo = "",
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = "",
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = hareket.IslemAdi,
                    IslemKodu = hareket.IslemKodu,
                    HesapCinsi = "",
                    SonBakiye = hareket.IslemSonrasıBakiye.ToString(),
                    IslemKanali = hareket.IslemKanal,
                    IslemOncesiBakiye = hareket.IslemOncesiBakiye.ToString(),
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.IslemTarihZamani,
                    Vkn = ""
                });
            }

            return list;
        }
    }
}