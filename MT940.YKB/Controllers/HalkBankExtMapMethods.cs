﻿
using System;
using System.Collections.Generic;
using System.Text;
using MT940.YKB.Controllers;
using MT940.YKB.Models;

namespace ExternalService.Services.Concrete.HalkBankasi.Utils
{
    public static class HalkBankExtMapMethods
    {
        public static string ToMapAmount(this string amount)
        {
            amount = amount.Replace(",", ".").Replace("+","").Replace("-","");
            return amount;
        }
        
        public static DateTime toFormatDate(this string date,string time) 
        {
            return new DateTime(
                Convert.ToInt32(date.Substring(0,4)),
                Convert.ToInt32(date.Substring(4, 2)),
                Convert.ToInt32(date.Substring(6, 2)),
                Convert.ToInt32(time.Substring(0, 2)),
                Convert.ToInt32(time.Substring(2, 2)),
                Convert.ToInt32(time.Substring(4, 2))
                );
        }
        public static string ToManipulateShkzg(this string value) {

            switch (value.Substring(0,1))
            {
                case "+":
                    return "H";
                case "-":
                    return "S";
                default:
                    return "H";
            }
        }
        public static string toFormatDate2(this string date, string time)
        {
            return date.Substring(6, 4) + date.Substring(3, 2) + date.Substring(0, 2) + time.Substring(0, 2) + time.Substring(3, 2) + time.Substring(6, 2);
        }

        public static List<Hesap> toMapAccount(this MT940.YKB.tr.com.halkbank.api.Hesap[] hesaps) {

            var list = new List<Hesap>();
            foreach (var account in hesaps)
            {
                var item = new Hesap()
                {
                    Hareketler = account.Hareketler?.ToMapHareket(),
                    Iban = account.IbanNo,
                    Bakiye = account.Bakiye?.ToMapAmount(),
                    HesapAcilisTarih = account.HesapAcilisTarihi?.ToFormatDateForResponse(),
                    SonHareketTarihi = account.SonHareketTarihi?.ToFormatDateForResponse(),
                    IslemiYapanSube = account.SubeKodu,
                    Blokemeblag = account.KrediLimit?.ToMapAmount(),
                    KullanilabilirBa = account.Bakiye.ToMapAmount(),
                    MusteriNo = account.MusteriNo,
                    AcilisIlkBakiye = "",
                    DovizTipi = "",
                    HesapNo = account.HesapNo,
                    KapanisBakiyesi = "",
                    SubeAdi = account.SubeAdi,
                    SubeKodu = account.SubeKodu,
                };

                list.Add(item);
            };
            return list;

        }
        public static List<Hareket> ToMapHareket(this MT940.YKB.tr.com.halkbank.api.Hareket[] harekets)
        {
            var list = new List<Hareket>();

            foreach (var hareket in harekets)
            {
                var item = new Hareket()
                {
                    Tutar = hareket.HareketTutari.Trim().ToMapAmount(),
                    Shkzg = hareket.HareketTutari.Trim().ToManipulateShkzg(),
                    Satir86 = hareket.EkstreAciklama,
                    CariBakiye = hareket.Bakiye.ToMapAmountCariBakiye(),
                    ValorTarihi = "",
                    IslemTarihi = hareket.Tarih.ToFormatDateForResponse(),
                    KarsiHesapIban = "",
                    Aciklama = hareket.EkstreAciklama,
                    TutarBorcAlacak = hareket.HareketTutari.StartsWith("-") ? "-" : "+",
                    FonksiyonKodu = hareket.IslemKod,
                    Mt940FonksiyonKo = hareket.IslemKod,
                    FisNo = hareket.DekontNo,
                    AmirVkn = hareket.KarsiHesapIBAN,
                    AmirTckn = hareket.KarsiKimlikNo,
                    HareketKey = hareket.ReferansNo,
                    MuhasebeTarih = hareket.Tarih.toFormatDate2(hareket.Saat),
                    SiraNo = hareket.Sirano,
                    KrediliKulBak = "",
                    KrediLimit = "",
                    ReferenceNo = hareket.ReferansNo,
                    GondAdi = "",
                    BorcluIban = "",
                    GondHesapNo = "",
                    Urf = "",
                    IslemAdi = "",
                    IslemKodu = hareket.IslemKod,
                    HesapCinsi = "",
                    SonBakiye = hareket.Bakiye.ToMapAmount(),
                    IslemKanali = "",
                    IslemOncesiBakiye = "",
                    HesapAdi = "",
                    AcilisGunBakiye = "",
                    KapanisBakiye = "",
                    Hata = "",
                    HesapTuru = "",
                    SonBakiyeBorcAla = "",
                    FaizOrani = "",
                    AlacakliIban = "",
                    AliciHesapNo = "",
                    HavaleRefNo = "",
                    VadeTarihi = "",
                    DigerMusteriNo = "",
                    GondSubeKodu = "",
                    HesapTuruKod = "",
                    Ekbilgi = "",
                    LastTmSt = hareket.DekontNo,
                    Vkn = ""
                };
                list.Add(item);
            }

            return list;
        }
    }
    
}
