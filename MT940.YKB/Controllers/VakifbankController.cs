﻿using ExternalService.Services.Concrete.VakifBank.Utils;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using MT940.YKB.LogInspector;
using MT940.YKB.Models;
using MT940.YKB.tr.com.vakifbank.api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace MT940.YKB.Controllers
{

    [System.Web.Mvc.Route("api/vakifbank")]
    public class VakifbankController : ApiController
    {

        SOnlineEkstreServisClient _client = null;

        [System.Web.Http.HttpPost]
        [System.Web.Mvc.Route("send")]
        public IHttpActionResult Send(ApiRequest<RequestObject> request)
        {
            
            var url = "https://vbservice.vakifbank.com.tr/HesapHareketleri.OnlineEkstre/SOnlineEkstreServis.svc";
            ConfigureLog4Net();
            CustomBinding binding2 = new CustomBinding();
            var security = TransportSecurityBindingElement.CreateUserNameOverTransportBindingElement();
            security.IncludeTimestamp = false;
            security.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256;
            security.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;

            var encoding = new TextMessageEncodingBindingElement();
            encoding.MessageVersion = MessageVersion.Soap11;

            var transport = new HttpsTransportBindingElement();
            transport.MaxReceivedMessageSize = 20000000; // 20 megs

            binding2.Elements.Add(security);
            binding2.Elements.Add(encoding);
            binding2.Elements.Add(transport);

            BasicHttpsBinding binding = new BasicHttpsBinding();
            binding.Security.Mode = BasicHttpsSecurityMode.TransportWithMessageCredential;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.OpenTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;

            _client = new SOnlineEkstreServisClient(binding, new EndpointAddress(url));
            // to use full client credential with Nonce uncomment this code:
            // it looks like this might not be required - the service seems to work without it
            //_client.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
            //_client.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());
            var myEndpointBehavior = new MyEndpointBehavior();
            _client.Endpoint.Behaviors.Add(myEndpointBehavior);

            _client.ClientCredentials.UserName.UserName = " ";
            _client.ClientCredentials.UserName.Password = " ";

            var response = new ApiResponse<ResponseObject>();
            try
            {
                var resp = _client.GetirHareketAsync(new DtoEkstreSorgu
                {
                    MusteriNo = "007295238186",
                    KurumKullanici = "AYGAZSEBNEM",
                    Sifre = "LFRDXMMB",
                    SorguBaslangicTarihi = request.RequestedObject.StartDate.toRequestDate(request.RequestedObject.StartTime),
                    SorguBitisTarihi = request.RequestedObject.EndDate.toRequestDate(request.RequestedObject.EndTime),
                    HesapNo = request.RequestedObject.AccountNumber
                }).Result;

                response = new ApiResponse<ResponseObject>
                {
                    ResponseObject = new ResponseObject
                    {
                        BankaAdi = "",
                        BankaKodu = "",
                        BankaVergiDairesi = "",
                        BankaVergiNumarasi = "",
                        HataAciklamasi = "",
                        HataKodu = "",
                        Solid = 0,
                        Hesaplar = resp.Hesaplar.ToMapAccount()
                    },
                    StatusCode = "0",
                    StatusMessage = "Successful."
                };
            }
            catch (Exception e)
            {
                response.StatusCode = "1";
                response.StatusMessage = e.Message + "\r InnerException : " + JsonConvert.SerializeObject(e);
            }
            return Ok(response);


        }
        private static void ConfigureLog4Net()
        {
            var appender = new FileAppender()
            {
                Layout = new SimpleLayout(),
                File = Assembly.GetExecutingAssembly().Location + ".log",
                Encoding = Encoding.UTF8,
                AppendToFile = true,
                LockingModel = new FileAppender.MinimalLock()
            };
            appender.ActivateOptions();
            BasicConfigurator.Configure(appender);
        }
        protected X509Certificate2 GetCertificate(string cer)
        {
            X509Certificate2 cert;
            var path = AppDomain.CurrentDomain.BaseDirectory;
            cert = new X509Certificate2(path + cer);
            return cert;

        }
    }
}