﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using MT940.YKB.LogInspector;
using MT940.YKB.Models;
using MT940.YKB.tr.com.yapikredi.api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Web;
using System.Web.Http;

namespace MT940.YKB.Controllers
{
    [System.Web.Mvc.Route("api/ykb")]
    public class YkbController : ApiController
    {
        public YkbController()
        {

        }
        private static ILog Logger
        {
            get { return _Logger ?? (_Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)); }
        }
        private static ILog _Logger;
        private EhoAccountTransactionServiceClient client;

        [System.Web.Http.HttpPost]
        [System.Web.Mvc.Route("send")]
        public async System.Threading.Tasks.Task<IHttpActionResult> SendAsync(ApiRequest<RequestObject> request)
        {
            try
            {

                var url = "https://dpextprd.yapikredi.com.tr:443/Hmn/EhoAccountTransactionService";
            
                //ConfigureLog4Net();

                CustomBinding binding2 = new CustomBinding();

                var security = TransportSecurityBindingElement.CreateUserNameOverTransportBindingElement();
                security.IncludeTimestamp = false;
                security.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256;
                security.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;

                var encoding = new TextMessageEncodingBindingElement();
                encoding.MessageVersion = MessageVersion.Soap11;

                var transport = new HttpsTransportBindingElement();
                transport.MaxReceivedMessageSize = 20000000; // 20 megs

                binding2.Elements.Add(security);
                binding2.Elements.Add(encoding);
                binding2.Elements.Add(transport);

                client = new EhoAccountTransactionServiceClient(binding2, new EndpointAddress(url));

                // to use full client credential with Nonce uncomment this code:
                // it looks like this might not be required - the service seems to work without it
                client.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
                client.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());
                var myEndpointBehavior = new MyEndpointBehavior();
                client.Endpoint.Behaviors.Add(myEndpointBehavior);

                client.ClientCredentials.UserName.UserName = request.RequestedObject.Param2;
                client.ClientCredentials.UserName.Password = request.RequestedObject.Param3;

                var resp = await client.sorgulaAsync(new requestEhoAccountTransaction()
                {
                    baslangicSaat = request.RequestedObject.StartTime.Substring(0, 4),
                    baslangicTarih = request.RequestedObject.StartDate,
                    bitisSaat = request.RequestedObject.EndTime.Substring(0, 4),
                    bitisTarih = request.RequestedObject.EndDate,
                    dovizKodu = request.RequestedObject.CurrencyCode,
                    firmaKodu = request.RequestedObject.Param1,
                    hesapNo = request.RequestedObject.AccountNumber
                });

                var response = new ApiResponse<ResponseObject>
                {
                    ResponseObject = new ResponseObject()
                    {
                        Solid = resp.@return.id,
                        Hesaplar = resp.@return.hesaplar?.hesap.ToMapHesap()
                    },
                    StatusCode = resp.@return.hataKodu,
                    StatusMessage = resp.@return.hataAciklamasi
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                //Logger.Error(ex);
                return BadRequest(ex.InnerException.Message);
            }

        }
        private static void ConfigureLog4Net()
        {
            var appender = new FileAppender()
            {
                Layout = new SimpleLayout(),
                File = AppDomain.CurrentDomain.BaseDirectory + "\\internal" + ".log",
                Encoding = Encoding.UTF8,
                AppendToFile = true,
                LockingModel = new FileAppender.MinimalLock()
            };
            appender.ActivateOptions();
            BasicConfigurator.Configure(appender);
        }
        protected X509Certificate2 GetCertificate(string cer)
        {
            X509Certificate2 cert;
            var path = AppDomain.CurrentDomain.BaseDirectory;
            cert = new X509Certificate2(path + cer);
            return cert;

        }
    }
}
