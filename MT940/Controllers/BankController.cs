﻿using System;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Web.Http;
using ExternalService.Model;
using ExternalServices.Model;
using log4net;
using LoggerService;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MT940.api.Helpers;
using MT940.api.Manager.Abstract;
using MT940.Manager.Concrete;
using NLog;
using AuthorizeAttribute = Microsoft.AspNetCore.Authorization.AuthorizeAttribute;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;

namespace MT940.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private IBankManager _manager;
        IConfiguration _configuration;
        private ILoggerManager _logger;

        public BankController(IConfiguration configuration, ILoggerManager logger)
        {
            _logger = logger;
            _configuration = configuration;
        }
        // POST: api/Bank
        [HttpPost]
        public ApiResponse<ResponseObject> Post([FromBody] ApiRequest<RequestObject> request)
        {
            var response = new ApiResponse<ResponseObject>();
            
            try
            {
                _manager = new BankManager(HttpContext.User, _configuration);
                response = _manager.Execute(request);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                response.StatusCode = "1";
                response.StatusMessage =e.Message + "/////Stack Trace====>"+ e.StackTrace;
            }

            return response;
        }
    }
}
