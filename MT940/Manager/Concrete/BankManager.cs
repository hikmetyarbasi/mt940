﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ExternalService.Factory.Concrete;
using ExternalService.Model;
using ExternalService.Services.Concrete.VakifBank;
using ExternalServices.Factory.Abstract;
using ExternalServices.Factory.Concrete;
using ExternalServices.Model;
using Microsoft.Extensions.Configuration;
using MT940.api.Manager.Abstract;

namespace MT940.Manager.Concrete
{
    public class BankManager : IBankManager
    {
        private readonly BankFactory _manager;
        private IEnumerable<Claim> _user;
        private GbUser _garantiUserInfo;
        private int bankType;

        public BankManager(ClaimsPrincipal user, IConfiguration configuration)
        {
            _user = user?.Claims;
            bankType = Convert.ToInt32(_user?.SingleOrDefault(p => p.Type == "BankType")?.Value);
            switch (bankType)
            {
                case (int)BankTypeEnum.Yapikredi:
                    _manager = new YapikrediFactory(configuration);
                    break;
                case (int)BankTypeEnum.Garanti:
                    _manager = new GarantiFactory();
                    break;
                case (int)BankTypeEnum.IsBankası:
                    _manager = new ISBankFactory();
                    break;
                case (int)BankTypeEnum.Sekerbank:
                    _manager = new SekerbankFactory();
                    break;
                case (int)BankTypeEnum.Halkbankasi:
                    _manager = new HalkbankFactory(configuration);
                    break;
                case (int)BankTypeEnum.Finansbank:
                    _manager = new FinansbankFactory();
                    break;
                case (int)BankTypeEnum.Akbank:
                    _manager = new AkbankFactory();
                    break;
                case (int)BankTypeEnum.Denizbank:
                    _manager = new DenizbankFactory();
                    break;
                case (int)BankTypeEnum.ZiraatBankasi:
                    _manager = new ZiraatFactory();
                    break;
                case (int)BankTypeEnum.TEBBankasi:
                    _manager = new TebFactory();
                    break;
                case (int)BankTypeEnum.Vakifbank:
                    _manager = new VakifBankFactory(configuration);
                    break;
                case (int)BankTypeEnum.Hsbc:
                    _manager = new HsbcFactory();
                    break;
                case (int)BankTypeEnum.Albaraka:
                    _manager = new AlbarakaFactory();
                    break;
                default:
                    throw new Exception("Invalid BankType Value");
            }
        }

        public ApiResponse<ResponseObject> Execute(ApiRequest<RequestObject> request)
        {
            ApiResponse<ResponseObject> response = null;
            var service = _manager.GetService(_user);

            int tryCount = 0;
            int maxTryCount = 1;
            if (service.GetType().Name == typeof(VakifBankService).Name)
                maxTryCount = 8;

            while (response?.StatusCode != "0" && tryCount < maxTryCount)
            {
                response = service.Send(request);
                tryCount++;
            }
            response.TryCount = tryCount;
            return response;
        }

    }
}
