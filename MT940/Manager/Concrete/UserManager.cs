﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ExternalService.Helpers;
using ExternalService.Model;
using ExternalService.Model.Finansbank;
using ExternalService.Model.IsBank;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MT940.api.Helpers;
using MT940.api.Manager.Abstract;

namespace MT940.Manager.Concrete
{
    public class UserManager : IUserManager
    {

        private readonly AppSettings _appSettings;
        private static GarantiUserInfo _garantiUserInfo;
        private static FinansbankUserInfo _finansbankUserInfo;
        private static IsBankaUserInfo _isBankaUser;
        private static IsBankaUserInfo _vakifBankUser;
        private static IsBankaUserInfo _denizbank;
        private static IsBankaUserInfo _ziraatbank;
        private static IsBankaUserInfo _ykbUser;
        private static IsBankaUserInfo _akbankUser;
        private static IsBankaUserInfo _hsbcUser;
        private static IsBankaUserInfo _albarakaUser;
        private AppUserInfo _appUsers;

        public UserManager(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            _garantiUserInfo = Helper.LoadJson<GarantiUserInfo>("JsonData\\garantiusers.json");
            _finansbankUserInfo = Helper.LoadJson<FinansbankUserInfo>("JsonData\\finansbankusers.json");
            _appUsers = Helper.LoadJson<AppUserInfo>("JsonData\\appusers.json");
            _isBankaUser = Helper.LoadJson<IsBankaUserInfo>("JsonData\\isbankasiuser.json");
            _denizbank = Helper.LoadJson<IsBankaUserInfo>("JsonData\\denizbankuser.json");
            _ziraatbank = Helper.LoadJson<IsBankaUserInfo>("JsonData\\ziraatuser.json");
            _vakifBankUser = Helper.LoadJson<IsBankaUserInfo>("JsonData\\vakifbankuser.json");
            _ykbUser = Helper.LoadJson<IsBankaUserInfo>("JsonData\\ykbuser.json");
            _akbankUser = Helper.LoadJson<IsBankaUserInfo>("JsonData\\akbankuser.json");
            _hsbcUser = Helper.LoadJson<IsBankaUserInfo>("JsonData\\hsbcuser.json");
            _albarakaUser = Helper.LoadJson<IsBankaUserInfo>("JsonData\\albarakauser.json");
        }

        public User Authenticate(string username, string password)
        {
            var user = _appUsers.Users.SingleOrDefault(x => x.Username == username && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var claimList = new List<Claim>();
            claimList.Add(new Claim("BankType", _appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType));


            //Token içerisinde extra bilgi tutulmak istendiğin banka özelinde eklenebilir.
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
               (int)BankTypeEnum.Finansbank)
            {
                claimList.Add(new Claim("FirmCode", _finansbankUserInfo.Users.FirstOrDefault(x => x.AppUserName == username)?.Details?.FirmCode));
            }

            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
                (int)BankTypeEnum.Garanti)
            {
                claimList.Add(new Claim("Username", _garantiUserInfo.Users.FirstOrDefault(x => x.AppUserName == username)?.Details.Username));
                claimList.Add(new Claim("Token", _garantiUserInfo.Users.FirstOrDefault(x => x.AppUserName == username)?.Details?.Token));
                claimList.Add(new Claim("FirmCode", _garantiUserInfo.Users.FirstOrDefault(x => x.AppUserName == username)?.Details?.FirmCode));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
                (int)BankTypeEnum.IsBankası)
            {
                claimList.Add(new Claim("MusteriNo", _isBankaUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.MusteriNo));
                claimList.Add(new Claim("UserName", _isBankaUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _isBankaUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
                (int)BankTypeEnum.Vakifbank)
            {
                claimList.Add(new Claim("MusteriNo", _vakifBankUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.MusteriNo));
                claimList.Add(new Claim("UserName", _vakifBankUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _vakifBankUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
              (int)BankTypeEnum.Denizbank)
            {
                claimList.Add(new Claim("UserName", _denizbank.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _denizbank.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
              (int)BankTypeEnum.ZiraatBankasi)
            {
                claimList.Add(new Claim("UserName", _ziraatbank.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _ziraatbank.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
              (int)BankTypeEnum.Yapikredi)
            {
                claimList.Add(new Claim("UserName", _ykbUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _ykbUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
              (int)BankTypeEnum.Akbank)
            {
                claimList.Add(new Claim("UserName", _akbankUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _akbankUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
             (int)BankTypeEnum.Hsbc)
            {
                claimList.Add(new Claim("UserName", _hsbcUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _hsbcUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
                claimList.Add(new Claim("AssociationCode", _hsbcUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.MusteriNo));
            }
            if (Convert.ToInt32(_appUsers.Users.FirstOrDefault(x => x.Username == username)?.BankType) ==
             (int)BankTypeEnum.Albaraka)
            {
                claimList.Add(new Claim("UserName", _albarakaUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.UserName));
                claimList.Add(new Claim("Password", _albarakaUser.Users.FirstOrDefault(x => x.AppUserName == username)?.UserDetail.Password));
            }
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claimList.ToArray()),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }
        public IEnumerable<User> GetAll()
        {
            // return users without passwords
            return _appUsers.Users.Select(x =>
            {
                x.Password = null;
                return x;
            });
        }



    }
}

