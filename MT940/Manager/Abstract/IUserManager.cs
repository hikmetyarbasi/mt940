﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExternalService.Model;
using ExternalServices.Model;

namespace MT940.api.Manager.Abstract
{
    public interface IUserManager
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
    }
}
